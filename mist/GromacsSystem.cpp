/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file GromacsSystem.cpp
* @brief Implementation of System interface for Gromacs
*/

#include "GromacsSystem.h"
#include <stddef.h>
#include <strings.h>
#include <iostream>

// Implementation of the Gromacs System class
// Designed for use with GROMACS 5.0.2

GromacsSystem::GromacsSystem() : System()
{
    callback = NULL;
    force_p = NULL;

    // Gromacs specific conversion factors
    boltzmann = 0.0083144621; // internal units are kJ/mol/K
    picosecond = 1.0;         // internal units are ps
    unitmass = 1.0;           // internal units are AMU
    angstrom = 0.1;           // internal units are nm
}

GromacsSystem::~GromacsSystem() {}

Vector3 GromacsSystem::GetVelocity(int i) const
{
    real x = ((real *)velocities)[3 * i];
    real y = ((real *)velocities)[3 * i + 1];
    real z = ((real *)velocities)[3 * i + 2];
    return Vector3(x, y, z);
}

void GromacsSystem::SetVelocity(int i, Vector3 v)
{
    ((real *)velocities)[3 * i] = v.x;
    ((real *)velocities)[3 * i + 1] = v.y;
    ((real *)velocities)[3 * i + 2] = v.z;
}

Vector3 GromacsSystem::GetPosition(int i) const
{
    real x = ((real *)positions)[3 * i];
    real y = ((real *)positions)[3 * i + 1];
    real z = ((real *)positions)[3 * i + 2];
    return Vector3(x, y, z);
}

void GromacsSystem::SetPosition(int i, Vector3 p)
{
    ((real *)positions)[3 * i] = p.x;
    ((real *)positions)[3 * i + 1] = p.y;
    ((real *)positions)[3 * i + 2] = p.z;
}

double GromacsSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double GromacsSystem::GetPotentialEnergy(force_component_t t) const
// Set variables needed by GROMACS to compute force component
{
    double e;
    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_EPOT];
        break;
    case (MIST_FORCE_BOND):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_G96BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_MORSE] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CUBICBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CONNBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_HARMONIC] +
            ((gmx_enerdata_t *)potential_energy)->term[F_FENEBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABBONDSNC] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRBONDS];
        break;
    case (MIST_FORCE_BEND):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_G96ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_LINEAR_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CROSS_BOND_BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CROSS_BOND_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_UREY_BRADLEY] +
            ((gmx_enerdata_t *)potential_energy)->term[F_QUARTIC_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABANGLES];
        break;
    case (MIST_FORCE_PROPDIHED):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_PDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RBDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CBTDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_FOURDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CMAP] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABDIHS];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_IDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_PIDIHS];
        break;
    case (MIST_FORCE_NONBONDED):
        e = GetPotentialEnergy(MIST_FORCE_ALL) -
            GetPotentialEnergy(MIST_FORCE_PROPDIHED) -
            GetPotentialEnergy(MIST_FORCE_IMPROPDIHED) -
            GetPotentialEnergy(MIST_FORCE_BEND) -
            GetPotentialEnergy(MIST_FORCE_BOND);
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 GromacsSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 GromacsSystem::GetForce(int i, force_component_t t) const
{
    real x, y, z;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        x = ((force_arrays_t *)forces)->f_total[i][0];
        y = ((force_arrays_t *)forces)->f_total[i][1];
        z = ((force_arrays_t *)forces)->f_total[i][2];
        break;
    case (MIST_FORCE_BOND):
        x = ((force_arrays_t *)forces)->f_bond[i][0];
        y = ((force_arrays_t *)forces)->f_bond[i][1];
        z = ((force_arrays_t *)forces)->f_bond[i][2];
        break;
    case (MIST_FORCE_BEND):
        x = ((force_arrays_t *)forces)->f_angle[i][0];
        y = ((force_arrays_t *)forces)->f_angle[i][1];
        z = ((force_arrays_t *)forces)->f_angle[i][2];
        break;
    case (MIST_FORCE_PROPDIHED):
        x = ((force_arrays_t *)forces)->f_propdihed[i][0];
        y = ((force_arrays_t *)forces)->f_propdihed[i][1];
        z = ((force_arrays_t *)forces)->f_propdihed[i][2];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        x = ((force_arrays_t *)forces)->f_impropdihed[i][0];
        y = ((force_arrays_t *)forces)->f_impropdihed[i][1];
        z = ((force_arrays_t *)forces)->f_impropdihed[i][2];
        break;
    case (MIST_FORCE_NONBONDED):
        x = ((force_arrays_t *)forces)->f_nonbonded[i][0];
        y = ((force_arrays_t *)forces)->f_nonbonded[i][1];
        z = ((force_arrays_t *)forces)->f_nonbonded[i][2];
        break;
    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

double GromacsSystem::GetMass(int i) const
{
    return 1. / (((real *)masses)[i]);
}

double GromacsSystem::GetInverseMass(int i) const
{
    return (((real *)masses)[i]);
}

char *GromacsSystem::GetKind(int i) const
{
    int count = 0;
    t_atoms *tat;
    gmx_mtop_t *top = (gmx_mtop_t *)kinds;

    for (int iblock = 0; iblock < top->nmolblock; iblock++)
    {
        tat = &top->moltype[top->molblock[iblock].type].atoms;
        for (int imols = 0; imols < top->molblock[iblock].nmol; imols++)
        {
            for (int iatoms = 0; iatoms < tat->nr; iatoms++)
            {
                int prevCount = count;
                count += 1; // Number of atoms in this molecule
                if (prevCount == i)
                {
                    return (char *)(*(tat->atomname[iatoms]));
                }
            }
        }
    }
}

void GromacsSystem::SetForceCallback(void (*c)(force_params_t *,
                                               force_arrays_t *),
                                     force_params_t *p, force_arrays_t *fc)
{
    callback = c;
    force_p = p;
    force_c = fc;
}

void GromacsSystem::UpdateForces()
{
    if (callback != NULL)
    {
        callback(force_p, force_c);
    }
}
