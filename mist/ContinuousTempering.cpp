/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/*
* @file ContinuousTempering.cpp
* @brief Implementation of Continuous Tempering Integrator
* @author GG
*/

#include "ContinuousTempering.h"
#include <iostream>

#include <math.h>
#include <cmath>
#include <stdio.h>
#include <fstream>
#include <string>
#include <iomanip>
#include <stdlib.h>

ContinuousTempering::ContinuousTempering(Param *p) : Integrator(p)
{
    // Default values
    friction = FRICTION_DEFAULT;
    temp = TEMP_DEFAULT;
    seed = SEED_DEFAULT;
    Tfact_inp = TFACT_DEFAULT;
    pe_offset_inp = PEOFFSET_DEFAULT;
    m_ss_inp = MSS_DEFAULT; 
    fric_ss_inp = FRICTION_DEFAULT;
    Delta_inp = DELTA_DEFAULT;
    Delta2_inp = DELTA2_DEFAULT;
    DeltaWall_inp = DELTAW_DEFAULT;
    hills_height_inp = HILLS_HEIGHT_DEFAULT;
    hills_width_inp = HILLS_WIDTH_DEFAULT;
    meta_dep = META_DEP_DEFAULT;
    save_conf = SAVE_CONF_DEFAULT;
    read_bias_file = 0;
    int err;
    features |= MIST_FEATURE_FORCE_COMPONENTS;

    std::cout << "MIST: Using Continuous Tempering" << std::endl;

    err = getParamDoubleValue(p, "langfriction", &friction);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for friction in input file, "
                     "choosing default value: " << friction << "(ps)^-1"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for friction is: " << friction << "(ps)^-1"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for friction. " << std::endl;
    }

    err = getParamDoubleValue(p, "langtemp", &temp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for temperature in input file, "
                     "choosing default value: " << temp << "K" << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for temperature is: " << temp << "K"
                  << std::endl;
    }
    else
    {
        std::cout
            << "MIST: Unknown error for getParamDoubleValue(), called for temp "
            << std::endl;
    }

    err = getParamDoubleValue(p, "temp_fact", &Tfact_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for temperature rescaling factor "
                     "in input file, "
                     "choosing default value: " << Tfact_inp << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for temperature rescaling factor is: "
                  << Tfact_inp << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for temp_fact " << std::endl;
    }

    err = getParamDoubleValue(p, "mass_xi", &m_ss_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for mass of additional DOF in "
                     "input file, "
                     "choosing default value: " << m_ss_inp << " UNITS ??"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for mass of additional DOF is: " << m_ss_inp
                  << " UNITS ??" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for m_ss. " << std::endl;
    }

    err = getParamDoubleValue(p, "langfriction_xi", &fric_ss_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for friction of additional DOF "
                     "in input file, "
                     "choosing default value: " << fric_ss_inp << "(ps)^-1"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for friction of additional DOF is: "
                  << fric_ss_inp << "(ps)^-1" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for langfriction_xi. " << std::endl;
    }

    err = getParamDoubleValue(p, "delta", &Delta_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for interval of additional DOF "
                     "for which value of the coupling function is zero in "
                     "input file, "
                     "choosing default value: " << Delta_inp << " UNITS ??"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for magnitude of additional DOF such that "
                     "value of coupling "
                     "function is zero: " << Delta_inp << " UNITS ??"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for Delta. " << std::endl;
    }

    err = getParamDoubleValue(p, "delta2", &Delta2_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for interval of additional DOF "
                     "for which the coupling function reaches the maximum in "
                     "input file, "
                     "choosing default value: " << Delta2_inp << " UNITS ??"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Length of interval in which coupling function goes "
                     "from zero "
                     "the maximum value: " << Delta2_inp << " UNITS ??"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for Delta2. " << std::endl;
    }

    err = getParamDoubleValue(p, "delta-wall", &DeltaWall_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for tuning shape of wall "
                     "confining additional DOF "
                     "in input file, "
                     "choosing default value: " << DeltaWall_inp << " UNITS ??"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout
            << "MIST: Value for tuning shape of wall confining additional DOF :"
            << DeltaWall_inp << " UNITS ??" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for DeltaWall. " << std::endl;
    }

    err = getParamDoubleValue(p, "hills-height", &hills_height_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout
            << "MIST: No value provided for hills height for metadynamics "
               "on additional DOF in input file, "
               "choosing default value: " << hills_height_inp << " UNITS ??"
            << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for hills height for metadynamics on "
                     "additional DOF :" << hills_height_inp << " UNITS ??"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for hills_height_inp. " << std::endl;
    }

    err = getParamDoubleValue(p, "hills-width", &hills_width_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for hills width for metadynamics "
                     "on additional DOF in input file, "
                     "choosing default value: " << hills_width_inp
                  << " UNITS ??" << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for hills width for metadynamics on "
                     "additional DOF :" << hills_width_inp << " UNITS ??"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for hills_width_inp. " << std::endl;
    }

    err = getParamIntValue(p, "metadyn-pace", (int *)&meta_dep);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for pace of hills deposition in "
                     "input file, "
                     "choosing default value: " << meta_dep << " time steps"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for pace of hills deposition is: " << meta_dep
                  << " time steps" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamIntValue(), called for "
                     "meta_dep " << std::endl;
    }

    err = getParamIntValue(p, "save-config-freq", (int *)&save_conf);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for frequency of saving "
                     "additional DOF configuration in input file, "
                     "choosing default value: " << save_conf << " time steps"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Saving additional DOF configuration every "
                  << save_conf << " time steps" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamIntValue(), called for "
                     "save_conf " << std::endl;
    }

    //this must be changed in order to read a bool value in input
    err = getParamIntValue(p, "read-bias-file", (int *)&read_bias_file);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: Read initial bias file option not selected, "
                     "building bias potential from scratch "
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Starting bias potential will be read from file"
                     " meta.0" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamIntValue(), called for "
                     "read_bias_file " << std::endl;
    }

    err = getParamDoubleValue(p, "peoffset", &pe_offset_inp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for potential energy offset in "
                     "input file, "
                     "choosing default value: " << pe_offset_inp << " UNITS ??"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for potential energy offset is: "
                  << pe_offset_inp << " UNITS ??" << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for pe_offset " << std::endl;
    }

    err = getParamIntValue(p, "seed", (int *)&seed);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for seed in input file, choosing "
                     "default value: " << seed << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for seed is: " << seed << std::endl;
    }
    else
    {
        std::cout
            << "MIST: Unknown error for getParamDIntValue(), called for seed "
            << std::endl;
    }

    r = new MIST_Random(seed);
}

ContinuousTempering::~ContinuousTempering()
{
    if (r != NULL)
    {
        delete r;
    }
}

void ContinuousTempering::SetSystem(System *s)
{
    Integrator::SetSystem(s);
    kbt = temp * system->Boltzmann();
}

void ContinuousTempering::Step(double dt)
{
    // Langevin BAOAB Scheme (from CM's NAMD-lite implementation)
    kbt = temp *
          system->Boltzmann(); // kbt is in internal energy units of the code
    double gdt = friction / system->Picosecond() *
                 dt; // dimensionless friction constant (friction is in ps^{-1})
    double c1 = exp(-gdt);
    double c3 = sqrt(kbt * (1 - c1 * c1));
    // additional deg. of freed. (=ss) and other necessary var.
    static int iter = 0;
    static double ss = 0; //0;
    static double v_ss = 0;
    static double force_ss = 0; // Also this must be dec. as static since it is
    // needed for the first half (B) kick!
    static double coupl = 0,
                  d_coupl = 0; // static for the same reason as force_ss

    // next variables are now read as input parameters and should only be
    // rescaled by unit of measure factors
    double Delta = Delta_inp;
    double Delta2 = Delta2_inp;
    double DeltaWall = DeltaWall_inp;
    double Tfact = Tfact_inp;
    double pe_offset = pe_offset_inp; // should be: double pe_offset = pe_offset_inp
                             // *system->Boltzmann()
    double energy = 0;
    double m_ss = 0.0041 * system->UnitMass(); // should be: double m_ss =
                                               // m_ss_inp * system->UnitMass();
    double fric_ss = fric_ss_inp;

    // BAOAB xi variables and service variables
    double g_ssdt = fric_ss / system->Picosecond() *
                    dt; // dimensionless friction constant for xi
    double c1_ss = exp(-g_ssdt);
    double c3_ss = sqrt(kbt * (1 - c1_ss * c1_ss));
    double l_char_inv = 1 / system->Angstrom();  //this is 1/[1 angstrom] in internal units
    int i;
    double m, sqrtinvm;
    Vector3 v, p, f, ftemp;

    // Velocity half-step (B)
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m = system->GetMass(i);
        v = system->GetVelocity(i);
        f = system->GetForce(i);
        // ftemp = system->GetForce(i, System::MIST_FORCE_PROPDIHED);
        // if pure tempering Vextended = V(q)-coupl*V(q) hence f-> fx - coupl*f
        // (f is minus derivative of V(q)!)
        v.x += dt * 0.5 / m * (f.x - coupl * f.x);
        v.y += dt * 0.5 / m * (f.y - coupl * f.y);
        v.z += dt * 0.5 / m * (f.z - coupl * f.z);
        system->SetVelocity(i, v);
    }
    v_ss += dt * 0.5 / m_ss * force_ss * l_char_inv; //for unit consistency, need to divide force_ss by a length scale [in internal units]

    // Position half step (A)
    PositionStep(0.5 * dt);
    ss += l_char_inv * dt * 0.5 * v_ss;

    // Velocity update (O)
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m = system->GetMass(i);
        v = system->GetVelocity(i);
        sqrtinvm = sqrt(1.0 / m);
        v.x = c1 * v.x + sqrtinvm * c3 * r->random_gaussian();
        v.y = c1 * v.y + sqrtinvm * c3 * r->random_gaussian();
        v.z = c1 * v.z + sqrtinvm * c3 * r->random_gaussian();
        system->SetVelocity(i, v);
    }
    // Velocity update for xi
    v_ss = c1_ss * v_ss + sqrt(1.0 / m_ss) * c3_ss * r->random_gaussian();

    // Position half step (A)
    PositionStep(0.5 * dt);
    ss += l_char_inv * dt * 0.5 * v_ss;

    system->UpdateForces();

    // compute the force force_ss acting on xi
    SS_coupling(&coupl, &d_coupl, ss, Delta, Delta2, Tfact);
    force_ss =
        0; // must be initialized to zero as functions only add contributes
    static_SSforce(&force_ss, ss, d_coupl, Delta, DeltaWall, pe_offset);
    // compute metadynamics contribution to f_ss
    if (iter % meta_dep == 0)
        metadyn(&force_ss, ss, 1, Delta, Delta2);
    else
        metadyn(&force_ss, ss, 0, Delta, Delta2);

    // Velocity half-step (B)
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m = system->GetMass(i);
        v = system->GetVelocity(i);
        f = system->GetForce(i);
        // ftemp = system->GetForce(i, System::MIST_FORCE_ALL);
        // if pure tempering Vextended = V(q)-coupl*V(q) hence f-> fx - coupl*f
        // (f is derivative of V(q)!)
        v.x += dt * 0.5 / m * (f.x - coupl * f.x);
        v.y += dt * 0.5 / m * (f.y - coupl * f.y);
        v.z += dt * 0.5 / m * (f.z - coupl * f.z);
        system->SetVelocity(i, v);
    }
    v_ss += dt * 0.5 / m_ss * force_ss * l_char_inv; //for unit consistency, need to divide force_ss by a length scale [in internal units]
    if (iter % save_conf == save_conf-1)
        write_confxyz(coupl, ss, Delta, iter + 1, force_ss * l_char_inv, f.x);
    iter += 1;
}

void ContinuousTempering::SS_coupling(double *cc, double *d_cc, double s,
                                      double D, double D2, double Tf)
{
    if (std::abs(s) <= D)
    {
        *cc = 0;
        *d_cc = 0;
    }
    else if ((D < std::abs(s)) && (std::abs(s) < D + D2))
    {
        *cc = Tf *(3 * pow((std::abs(s) - D) / D2, 2) -
                   2 * pow((std::abs(s) - D) / D2, 3));
        if (s > 0)
            *d_cc = 6 * Tf *(((std::abs(s) - D) / D2) -
                             pow((std::abs(s) - D) / D2, 2)) /
                    D2;
        else
            *d_cc = -6 * Tf *(((std::abs(s) - D) / D2) -
                              pow((std::abs(s) - D) / D2, 2)) /
                    D2;
    }
    else
    {
        *cc = Tf;
        *d_cc = 0;
    }
}

void ContinuousTempering::static_SSforce(double *fs, double s, double d_cc,
                                         double D, double Dl, double peoff)
{
    // contrib. due to coupling
    double pe = system->GetPotentialEnergy(); // read potential energy
    *fs += d_cc *(pe - peoff); // V_extended_sys=(V-peoff)-cc*(V-peoff)
    // contrib. due to confining potential
    if (std::abs(s) > (D + Dl))
    {
        *fs += -(s / std::abs(s)) * 5.0 * kbt *
               ((std::abs(s) - (D + Dl)) * (std::abs(s) - (D + Dl)) / D) / D;
    }
    // test additional quasi-linear term to confining potential
}

void ContinuousTempering::metadyn(double *fs, double s, bool addgauss, double D,
                                  double D2)
{
    static double hills_height = hills_height_inp * system->Boltzmann(),
                  sigma = hills_width_inp; // height of gaussian in internal energy units
    const int metastride = 1000;//1000;  // how many gaussians are deposited before saving bias potential to file
    const int NG = 90000;
    static double VG[NG + 2] = {0};
    static int it = 0;
    const double smax = 1.03 * (D + D2);
    const double smin = -smax;
    const double dsg = (smax - smin) / (NG - 1);
    const double swp = smax + dsg;
    const double swm = smin - dsg;
    static bool isfirst = 1;

    //read input bias potential
    if(isfirst && (read_bias_file==1))
    {
        isfirst = 0;
        std::ifstream biasfile;
        biasfile.open("meta.0");
        int count_lines = 0;
        std::string inputline;
        while( getline (biasfile,inputline)) count_lines++;
        if((NG + 2)!=count_lines)
        {
            std::cout << "MIST: Incompatible number of lines in input bias potential file meta.0 "
                         "number of lines must equal the size of the internal grid which is " 
                         << NG+2 << std::endl;
            exit(0);
        }
        biasfile.clear();
        biasfile.seekg(0);
        for (int ig = 0; ig < NG + 2; ig++)
        {
            double cc;
            biasfile >> cc >> VG[ig];
        }
        biasfile.close();
    } 

    // add gaussian
    if (addgauss)
    {
        it = it + 1;
        for (int ig = 1; ig < NG + 1; ig++)
        {
            VG[ig] =
              VG[ig] +
              hills_height * exp(-0.5 * pow((s - (swm + (ig - 1) * dsg)) / sigma, 2)) +
              // adding the same hill on the symmetric position
              hills_height * exp(-0.5 * pow((-s - (swm + (ig - 1) * dsg)) / sigma, 2));
        }
        // write metadyn bias potential
        if (it % metastride == 0)
        {
            char filename[15];
            sprintf(filename, "meta.%d", (it / metastride));
            std::ofstream biasfile;
            biasfile.open(filename);
            for (int ig = 0; ig < NG + 2; ig++)
            {
                biasfile << std::setw(15) << swm + (ig - 1) * dsg
                         << std::setw(20) << VG[ig] << "\t" << s << "\n";
            }
            biasfile.close();
        }
    }

    // evaluate bias force
    if ((s > swm) && (s < swp))
    {
        // Bin evaluation
        int ig = ceil((s - swm) / dsg);
        // evaluate force
        if ((ig >= 1) && (ig < NG))
            *fs += -(VG[ig + 1] - VG[ig]) / dsg;
    }
}

void ContinuousTempering::write_confxyz(double cc, double s, double D, int it, double fss, double fx)
{
    // this function writes the position of xi and the value of the coupling
    // function
    // for the current frame into the file 'coupling.dat'. This happens every
    // save_conf
    // frames.
    static bool isfirst = 1;
    int i;
    static std::ofstream couplingoutfile;

    // output files initialization
    if (isfirst)
    {
        couplingoutfile.open("coupling.dat");
        isfirst = 0;
    }

    // write out position of xi (=s) and value of coupling function (=cc)
    double pe = system->GetPotentialEnergy(); // read potential energy
    couplingoutfile << it << "\t" << s << "\t" << cc << "\t" << pe << "\t" << fss << "\t" << fx << "\n";
    couplingoutfile.flush();
}

// Here is the write_conf function to write also xyz files with coordinates
// internally from MIST
// void ContinuousTempering::write_confxyz(double cc, double s, double D, int
// it)
//{
//    static bool isfirst = 1;
//    int i;
//    Vector3 p;
//    static std::ofstream allconfoutfile;
//    static std::ofstream canonicconfoutfile;
//
//    // output files initialization
//    if (isfirst)
//    {
//        allconfoutfile.open("all_conf.xyz");
//        canonicconfoutfile.open("canonic_conf.xyz");
//        isfirst = 0;
//    }
//
//    // write conf & if coupling=0 write conf in physical temp. conf. file
//    allconfoutfile << system->GetNumParticles() << "\n";
//    allconfoutfile << "\tss\t" << s << "\tcoupling\t" << cc << "\titeration\t"
//                   << it << "\n";
//    if (cc == 0)
//    {
//        canonicconfoutfile << system->GetNumParticles() << "\n";
//        canonicconfoutfile << "\tss\t" << s << "\tcoupling\t" << cc
//                           << "\titeration\t" << it << "\n";
//    }
//    for (i = 0; i < system->GetNumParticles(); i++)
//    {
//        p = system->GetPosition(i);
//        allconfoutfile << system->GetKind(i) << "\t";
//        allconfoutfile << p.x << "\t" << p.y << "\t" << p.z << "\n";
//        if (cc == 0)
//        {
//            canonicconfoutfile << system->GetKind(i) << "\t";
//            canonicconfoutfile << p.x << "\t" << p.y << "\t" << p.z << "\n";
//        }
//    }
//}
//
