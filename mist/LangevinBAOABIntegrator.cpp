/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file LangevinBAOABIntegrator.cpp
* @brief Implementation of the Langevin BAOAB Integrator
*/

#include "LangevinBAOABIntegrator.h"
#include <iostream>

#include <math.h>

LangevinBAOABIntegrator::LangevinBAOABIntegrator(Param *p) : Integrator(p)
{
    // Default values
    friction = FRICTION_DEFAULT;
    temp = TEMP_DEFAULT;
    seed = SEED_DEFAULT;
    int err;

    std::cout << "MIST: Using Langevin BAOAB Integrator" << std::endl;

    // Read in parameters from mist.params file
    err = getParamDoubleValue(p, "langfriction", &friction);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for friction in input file, "
                     "choosing default value: " << friction << "(ps)^-1"
                  << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for friction is: " << friction << "(ps)^-1"
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error for getParamDoubleValue(), called "
                     "for friction. " << std::endl;
    }

    err = getParamDoubleValue(p, "langtemp", &temp);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for temperature in input file, "
                     "choosing default value: " << temp << "K" << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for temperature is: " << temp << "K"
                  << std::endl;
    }
    else
    {
        std::cout
            << "MIST: Unknown error for getParamDoubleValue(), called for temp "
            << std::endl;
    }

    err = getParamIntValue(p, "seed", (int *)&seed);
    if (err == ERR_INPUTVALUE)
    {
        std::cout << "MIST: No value provided for seed in input file, choosing "
                     "default value: " << seed << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Value for seed is: " << seed << std::endl;
    }
    else
    {
        std::cout
            << "MIST: Unknown error for getParamDoubleValue(), called for seed "
            << std::endl;
    }

    r = new MIST_Random(seed);
}

LangevinBAOABIntegrator::~LangevinBAOABIntegrator()
{
    if (r != NULL)
    {
        delete r;
    }
}

void LangevinBAOABIntegrator::SetSystem(System *s)
{
    Integrator::SetSystem(s);
    kbt = temp * system->Boltzmann();
}

void LangevinBAOABIntegrator::Step(double dt)
{
    // Langevin BAOAB Scheme (from CM's NAMD-lite implementation)

    double gdt = friction / system->Picosecond() * dt;
    double c1 = exp(-gdt);
    double c3 = sqrt(kbt * (1 - c1 * c1));

    double m, sqrtinvm;
    Vector3 v;

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);

    // Position half step (A)
    PositionStep(0.5 * dt);

    // Velocity update (O)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        m = system->GetMass(i);
        v = system->GetVelocity(i);
        sqrtinvm = sqrt(1.0 / m);
        v.x = c1 * v.x + sqrtinvm * c3 * r->random_gaussian();
        v.y = c1 * v.y + sqrtinvm * c3 * r->random_gaussian();
        v.z = c1 * v.z + sqrtinvm * c3 * r->random_gaussian();
        system->SetVelocity(i, v);
    }

    // Position half step (A)
    PositionStep(0.5 * dt);

    system->UpdateForces();

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);
}
