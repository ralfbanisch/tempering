/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file LeapfrogIntegrator.cpp
* @brief Implementation of a Leapfrog integrator.
*/

#include "LeapfrogIntegrator.h"
#include "System.h"
#include <iostream>

LeapfrogIntegrator::LeapfrogIntegrator(Param *p) : Integrator(p)
{
    std::cout << "MIST: Leapfrog Integrator" << std::endl;
}

LeapfrogIntegrator::~LeapfrogIntegrator() {}

void LeapfrogIntegrator::Step(double dt)
{
    // Simple leapfrog algorithm

    // Position full step
    PositionStep(dt);

    // Update forces using x(t) and v(t-1/2 dt)
    system->UpdateForces();

    // Velocity full-step but out of sync with position
    VelocityStep(dt);
}
