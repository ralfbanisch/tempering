/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file mist.cpp
* @brief Implementation of the MIST library interface
*/

#include "mist.h"

#include "VerletIntegrator.h"
#include "LeapfrogIntegrator.h"
#include "LangevinBAOABIntegrator.h"
#include "Yoshida4Integrator.h"
#include "Yoshida8Integrator.h"
#include "ContinuousTempering.h"

#if defined(__MIST_WITH_NAMD_LITE)
#include "NAMDLiteSystem.h"
#elif defined(__MIST_WITH_GROMACS)
#include "GromacsSystem.h"
#endif

#include <strings.h>

#include <iostream>

#define MIST_PARAM_FILE "mist.params"
#define INTEGRATOR_TYPE_LABEL "integrator"

// Definition of the global integrator object (file scope)
static Integrator *integrator;
/**
* \brief this is the MIST_Init function
**/
int MIST_Init()
{
    int err;
    Param *p;
    char *type;

    // Read and allocate params
    err = readParams(MIST_PARAM_FILE, &p);

    if (err == ERR_OK)
    {
        std::cout << "MIST: Input file ok. " << std::endl;
    }
    else if (err == ERR_NOFILE)
    {
        std::cout << "MIST: No input file mist.params could be found."
                  << std::endl;
    }
    else
    {
        std::cout << "MIST: Error type: " << err << std::endl;
    }

    // Check which integrator type and initialise it
    err = getParamStringValue(p, INTEGRATOR_TYPE_LABEL, &type);
    if (err != ERR_OK)
    {
        std::cout << "MIST: No integrator type was identified." << std::endl;
    }
    else if (strcasecmp(type, "verlet") == 0)
    {
        integrator = new VerletIntegrator(p);
    }
    else if (strcasecmp(type, "leapfrog") == 0)
    {
        integrator = new LeapfrogIntegrator(p);
    }
    else if (strcasecmp(type, "langevin") == 0)
    {
        integrator = new LangevinBAOABIntegrator(p);
    }
    else if (strcasecmp(type, "yoshida4") == 0)
    {
        integrator = new Yoshida4Integrator(p);
    }
    else if (strcasecmp(type, "yoshida8") == 0)
    {
        integrator = new Yoshida8Integrator(p);
    }
    else if (strcasecmp(type, "tempering") == 0)
    {
        integrator = new ContinuousTempering(p);
    }
    else
    {
        std::cout << "MIST: Your choice (" << type
                  << ") for the integrator is unknown." << std::endl;
    }

    // Initialise the system object, attach it to the integrator
    System *s = NULL;
#if defined(__MIST_WITH_NAMD_LITE)
    s = new NAMDLiteSystem();
#elif defined(__MIST_WITH_GROMACS)
    s = new GromacsSystem();
#endif
    integrator->SetSystem(s);

    std::cout << "MIST: Initialised MIST Library" << std::endl;
    return 0;
}

int MIST_SetNumParticles(int n)
{
    integrator->GetSystem()->SetNumParticles(n);
    return 0;
}

int MIST_SetPositions(void *positions)
{
    integrator->GetSystem()->SetPositionPtr(positions);
    return 0;
}

int MIST_SetVelocities(void *velocities)
{
    integrator->GetSystem()->SetVelocityPtr(velocities);
    return 0;
}

int MIST_SetMasses(void *masses)
{
    integrator->GetSystem()->SetMassPtr(masses);
    return 0;
}

int MIST_SetKinds(void *types)
{
    integrator->GetSystem()->SetKindsPtr(types);
    return 0;
}

int MIST_SetPotentialEnergy(void *potential_energy)
{
    integrator->GetSystem()->SetPotentialEnergyPtr(potential_energy);
    return 0;
}

int MIST_SetForces(void *forces)
{
    integrator->GetSystem()->SetForcePtr(forces);
    return 0;
}

int MIST_GetFeatures() { return integrator->GetFeatures(); }

int MIST_Step(double dt)
{
    integrator->Step(dt);
    return 0;
}

int MIST_Cleanup()
{
    int err;
    if (integrator != NULL)
    {
        delete integrator;
        integrator = NULL;
    }

    std::cout << "MIST: Cleaned up MIST Library" << std::endl;
    return 0;
}

#if defined(__MIST_WITH_NAMD_LITE)
// Implementation of the NAMD-Lite specific force callback routine

int MIST_SetForceCallback(int (*callback)(Step *), Step *s)
{
    ((NAMDLiteSystem *)integrator->GetSystem())->SetForceCallback(callback, s);
    return 0;
}
#endif

#if defined(__MIST_WITH_GROMACS)
// Implementation of the Gromacs specific force callback routine

int MIST_SetForceCallback(void(callback)(force_params_t *, force_arrays_t *),
                          force_params_t *p, force_arrays_t *fc)
{
    ((GromacsSystem *)integrator->GetSystem())
        ->SetForceCallback(callback, p, fc);
    return 0;
}
#endif
