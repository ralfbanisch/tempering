/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Integrator.h
* @brief Declaration of general integrator base class (abstract)
*/

#ifndef MIST_INTEGRATOR_H
#define MIST_INTEGRATOR_H

extern "C" {
#include "params.h"
}

#include "System.h"
#include "mist.h"

/**
* @brief Declaration of the Integrator abstract base class, which it the
interface
* all other integrators inherit from.
*
* This is the interface that all integrators must implement. It provides an
abstract
* Step() function which will be called by the host application to integrate the
system
* forward in time.
*
* Several convenience functions are also provided that are commonly needed by
subclasses.
*/
class Integrator
{
  public:
    /**
    * @brief Default Constructor
    *
    * @param p a pointer to the parameters
    */
    Integrator(Param *p);

    /**
    * @brief Default Destructor
    */
    virtual ~Integrator();

    /**
    * @brief Computes an integration step from t to t+dt
    *
    * This function must be implemented by any subclasses, and computes a
    * single integration step from the current time (t) to (t+dt).
    *
    * @param dt the timestep (in internal units)
    */
    virtual void Step(double dt) = 0;

    /**
    * @brief Setter for the system pointer
    *
    * @param s pointer to an instance of the System class
    */
    virtual void SetSystem(System *s);

    /**
    * @brief Getter for the system pointer
    *
    * @return pointer to an instance of the System class
    */
    System *GetSystem() const;

    /**
    * @brief Getter for feature flags
    *
    * @return binary OR'ed combination of feature flags required by the Integrator
    */
    int GetFeatures() const;

  protected:
    /**
    * @brief Integrates all particle positions by dt
    *
    * @param dt the time interval to integrate over
    */
    void PositionStep(double dt);

    /**
    * @brief Integrates all particle velocities by dt
    *
    * @param dt the time interval to integrate over
    */
    void VelocityStep(double dt);

    Param *params;   /**< @brief Pointer to the user's parameters */
    System *system;  /**< @brief Pointer to the System to be integrated */
    int features;    /**< @brief Binary OR'ed combination of feature flags */
};

#endif
