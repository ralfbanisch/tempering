/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file LangevinBAOABIntegrator.h
* @brief Declaration of Langevin-BAOAB integrator class.
*/

#ifndef MIST_LANGEVINBAOABINTEGRATOR_H
#define MIST_LANGEVINBAOABINTEGRATOR_H

#define FRICTION_DEFAULT 1.0
#define TEMP_DEFAULT 300.0
#define SEED_DEFAULT 100

#include "Integrator.h"
#include "MIST_Random.h"

class LangevinBAOABIntegrator : public Integrator
{
  public:
    LangevinBAOABIntegrator(Param *p);
    virtual ~LangevinBAOABIntegrator();

    void Step(double dt);
    void SetSystem(System *s);

  protected:
    double friction;
    double temp;
    unsigned long seed;
    double kbt;
    MIST_Random *r;
};

#endif
