/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Yoshida8Integrator.cpp
* @brief Implementation of an eighth-order constant energy integrator
*/

#include "Yoshida8Integrator.h"
#include "System.h"
#include <iostream>

Yoshida8Integrator::Yoshida8Integrator(Param *p) : Integrator(p)
{
    std::cout << "MIST: Using Yoshida 8th order Integrator" << std::endl;
}

Yoshida8Integrator::~Yoshida8Integrator() {}

void Yoshida8Integrator::Step(double dt)
{
    // Yoshida 8th order algorithm

    int i, j, k;
    double m;
    double d[8] = {0.104242620869991e1,   0.182020630970714e1,
                   0.157739928123617e0,   0.244002732616735e1,
                   -0.716989419708120e-2, -0.244699182370524e1,
                   -0.161582374150097e1,  -0.17808286265894516e1};

    Vector3 v, p, f;

    for (k = 0; k < 15; k++)
    {
        if (k < 8)
            j = k;
        else
            j = 14 - k;

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);

        // Position full step
        PositionStep(dt * d[j]);

        system->UpdateForces();
        /*    Vector3 f;
              std::cout << "call " << k << std::endl;
            for (int i = 0 ; i < system->GetNumParticles(); i++){
              f = system->GetForce(i);
              std::cout << "Force on " << i << " " << f.x << " "<< f.y << " "<<
           f.z << std::endl;
            }*/

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);
    }
}
