/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file NAMDLiteSystem.cpp
* @brief Implementation of System interface for NAMD-Lite
*/

#include "NAMDLiteSystem.h"
#include <stddef.h>

#include "force/fresult.h"

// Implementation of the NAMD-Lite System class

NAMDLiteSystem::NAMDLiteSystem() : System()
{
    callback = NULL;
    step = NULL;

    // NAMD-Lite specific conversion factors
    boltzmann = 0.001987191;        // internal units are kcal/mol/K
    picosecond = 1000.0;            // internal units are fs
    unitmass = 2390.05736137667304; // internal units are amu, but stored masses
    // carry a factor of MD_FORCE_CONST (to obtain a consistent
    // set of units for integration)
    angstrom = 1.0; // internal units are angstrom
}

NAMDLiteSystem::~NAMDLiteSystem() {}

Vector3 NAMDLiteSystem::GetVelocity(int i) const
{
    double x = ((double *)velocities)[3 * i];
    double y = ((double *)velocities)[3 * i + 1];
    double z = ((double *)velocities)[3 * i + 2];
    return Vector3(x, y, z);
}

void NAMDLiteSystem::SetVelocity(int i, Vector3 v)
{
    ((double *)velocities)[3 * i] = v.x;
    ((double *)velocities)[3 * i + 1] = v.y;
    ((double *)velocities)[3 * i + 2] = v.z;
}

Vector3 NAMDLiteSystem::GetPosition(int i) const
{
    double x = ((double *)positions)[3 * i];
    double y = ((double *)positions)[3 * i + 1];
    double z = ((double *)positions)[3 * i + 2];
    return Vector3(x, y, z);
}

void NAMDLiteSystem::SetPosition(int i, Vector3 p)
{
    ((double *)positions)[3 * i] = p.x;
    ((double *)positions)[3 * i + 1] = p.y;
    ((double *)positions)[3 * i + 2] = p.z;
}

double NAMDLiteSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double NAMDLiteSystem::GetPotentialEnergy(force_component_t t) const
{
    double e;
    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = *(double *)potential_energy;
        break;
    case (MIST_FORCE_BOND):
        e = step->all_forces->u_bond;
        break;
    case (MIST_FORCE_BEND):
        e = step->all_forces->u_angle;
        break;
    case (MIST_FORCE_PROPDIHED):
        e = step->all_forces->u_dihed;
        break;
    case (MIST_FORCE_IMPROPDIHED):
        e = step->all_forces->u_impr;
        break;
    case (MIST_FORCE_NONBONDED):
        e = step->all_forces->u_elec + step->all_forces->u_vdw;
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 NAMDLiteSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 NAMDLiteSystem::GetForce(int i, force_component_t t) const
{
    double x, y, z;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        x = ((double *)forces)[3 * i];
        y = ((double *)forces)[3 * i + 1];
        z = ((double *)forces)[3 * i + 2];
        break;
    case (MIST_FORCE_BOND):
        x = ((double *)step->all_forces->f_bond)[3 * i];
        y = ((double *)step->all_forces->f_bond)[3 * i + 1];
        z = ((double *)step->all_forces->f_bond)[3 * i + 2];
        break;
    case (MIST_FORCE_BEND):
        x = ((double *)step->all_forces->f_angle)[3 * i];
        y = ((double *)step->all_forces->f_angle)[3 * i + 1];
        z = ((double *)step->all_forces->f_angle)[3 * i + 2];
        break;
    case (MIST_FORCE_PROPDIHED):
        x = ((double *)step->all_forces->f_dihed)[3 * i];
        y = ((double *)step->all_forces->f_dihed)[3 * i + 1];
        z = ((double *)step->all_forces->f_dihed)[3 * i + 2];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        x = ((double *)step->all_forces->f_impr)[3 * i];
        y = ((double *)step->all_forces->f_impr)[3 * i + 1];
        z = ((double *)step->all_forces->f_impr)[3 * i + 2];
        break;
    case (MIST_FORCE_NONBONDED):
        x = ((double *)step->all_forces->f_elec)[3 * i] +
            ((double *)step->all_forces->f_vdw)[3 * i];
        y = ((double *)step->all_forces->f_elec)[3 * i + 1] +
            ((double *)step->all_forces->f_vdw)[3 * i + 1];
        z = ((double *)step->all_forces->f_elec)[3 * i + 2] +
            ((double *)step->all_forces->f_vdw)[3 * i + 2];
        break;
    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

double NAMDLiteSystem::GetMass(int i) const { return ((double *)masses)[i]; }

double NAMDLiteSystem::GetInverseMass(int i) const
{
    return (1.0 / (((double *)masses)[i]));
}

char *NAMDLiteSystem::GetKind(int i) const
{
    return (char *)(((MD_Atom *)kinds)[i].type);
}

void NAMDLiteSystem::SetForceCallback(int (*c)(Step *), Step *s)
{
    callback = c;
    step = s;
}

void NAMDLiteSystem::UpdateForces()
{
    if (callback != NULL)
    {
        int ret = callback(step);
    }
}
