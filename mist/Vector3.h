/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Vector3.h
* @brief Declaration of a lightweight 3D vector class
* 
*/

#ifndef MIST_VECTOR3_H
#define MIST_VECTOR3_H

/**
* @brief A lightweight 3D vector class
*
* A 3D double-precision vector class.  Currently no additional operations are supported.
* Direct access to the members x,y,z is permitted.
*/
class Vector3
{
  public:
    /**
    * @brief Default Constructor
    *
    * Creates and zeros a vector
    */
    Vector3() : x(0.0), y(0.0), z(0.0){};
    
    /**
    * @brief Constructor with specified entries
    *
    * Creates a vector and sets it according to the provided x,y,z values
    *
    * @param x 1st entry in the vector
    * @param y 2nd entry in the vector
    * @param z 3rd entry in the vector
    */
    Vector3(double x, double y, double z) : x(x), y(y), z(z){};

    double x; /**< @brief 1st component of the vector, publically accessible */
    double y; /**< @brief 2nd component of the vector, publically accessible */
    double z; /**< @brief 3rd component of the vector, publically accessible */

};
#endif
