/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file ContinuousTempering.h
* @brief Declaration of class for Continuous Tempering Integrator
* @author GG
*/

#ifndef MIST_CONTINUOUSTEMPERING_H
#define MIST_CONTINUOUSTEMPERING_H

#define FRICTION_DEFAULT 1.0
#define TEMP_DEFAULT 300.0
#define SEED_DEFAULT 100
#define DELTA_DEFAULT 50
#define DELTA2_DEFAULT 100
#define DELTAW_DEFAULT 50
#define TFACT_DEFAULT 0.5
#define PEOFFSET_DEFAULT 0.0
#define MSS_DEFAULT 1
#define META_DEP_DEFAULT 1000
#define SAVE_CONF_DEFAULT 500
#define HILLS_HEIGHT_DEFAULT 7.5
#define HILLS_WIDTH_DEFAULT 3.05

#include "Integrator.h"
#include "MIST_Random.h"

class ContinuousTempering : public Integrator
{
  public:
    ContinuousTempering(Param *p);
    virtual ~ContinuousTempering();

    void Step(double dt);
    void SetSystem(System *s);

  protected:
    void SS_coupling(double *cc, double *d_cc, double s, double D, double D2,
                     double Tf);
    void static_SSforce(double *fs, double s, double d_cc, double D, double Dl,
                        double peoff);
    void metadyn(double *fs, double s, bool addgauss, double D, double D2);
    void write_confxyz(double cc, double s, double D, int it, double fss, double fx);

    double friction;
    double temp;
    unsigned long seed;
    double Tfact_inp;
    double pe_offset_inp;
    double m_ss_inp;
    double fric_ss_inp;
    double Delta_inp;
    double Delta2_inp;
    double DeltaWall_inp;
    double hills_height_inp;
    double hills_width_inp;
    int meta_dep;
    int save_conf;
    int read_bias_file; //this should be transformed into bool
    double kbt;
    MIST_Random *r;
};

#endif
