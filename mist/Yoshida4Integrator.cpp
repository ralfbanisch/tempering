/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Yoshida4Integrator.cpp
* @brief Implementation of a fourth-order constant energy integrator
*/

#include "Yoshida4Integrator.h"
#include "System.h"
#include <iostream>

Yoshida4Integrator::Yoshida4Integrator(Param *p) : Integrator(p)
{
    std::cout << "MIST: Using Yoshida 4th order Integrator" << std::endl;
}

Yoshida4Integrator::~Yoshida4Integrator() {}

void Yoshida4Integrator::Step(double dt)
{
    // Yoshida 4th order algorithm

    int i, j;
    double m;
    double d[3] = {1.351207192, -1.702414384, 1.351207192};
    Vector3 v, p, f;

    for (j = 0; j < 3; j++)
    {
        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);

        // Position full step
        PositionStep(dt * d[j]);

        system->UpdateForces();

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);
    }
}
