/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file VerletIntegrator.cpp
* @brief Implementation of a Verlet Integrator
*/

#include "VerletIntegrator.h"
#include "System.h"
#include <iostream>

VerletIntegrator::VerletIntegrator(Param *p) : Integrator(p)
{
    std::cout << "MIST: Using Velocity Verlet Integrator" << std::endl;
}

VerletIntegrator::~VerletIntegrator() {}

void VerletIntegrator::Step(double dt)
{

    // Simple velocity verlet algorithm

    // Velocity half-step
    VelocityStep(0.5 * dt);

    // Position full step
    PositionStep(dt);

    system->UpdateForces();

    // Velocity half-step
    VelocityStep(0.5 * dt);
}
