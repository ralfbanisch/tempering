/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file GromacsSystem.h
* @brief Declaration of specific system class for Gromacs
*/

#ifndef MIST_GROMACS_SYSTEM_H
#define MIST_GROMACS_SYSTEM_H

#include "System.h"
#include "gromacs/legacyheaders/types/mist_forcecallback.h"

/**
* @brief A System class which provides accessors for particle data inside the Gromacs code
*
* This is a specialisation of the System interface which is specific to the Gromacs code.  It provides access to an Integrator to the particle data stored in various arrays within Gromacs, and also the ability to compute updates forces, calling Gromacs force evaluation function by means of a callback.
*
*/
class GromacsSystem : public System
{
  public:
    /**
    * @brief Default Constructor
    *
    * Sets the Gromacs-specific unit conversion factors:
    * - Energy in kJ/mol
    * - Time in ps
    * - Mass in AMU
    * - Length in Angstrom
    *
    */
    GromacsSystem();

    /**
    * @brief Default Destructor
    *
    * Does nothing
    */
    ~GromacsSystem();

    // Interface to Integrators
    Vector3 GetVelocity(int i) const;
    void SetVelocity(int i, Vector3 v);

    Vector3 GetPosition(int i) const;
    void SetPosition(int i, Vector3 p);

    Vector3 GetForce(int i) const;
    Vector3 GetForce(int i, force_component_t t) const;
    double GetMass(int i) const;
    double GetInverseMass(int i) const;
    double GetPotentialEnergy() const;
    double GetPotentialEnergy(force_component_t t) const;
    char *GetKind(int i) const;

    void UpdateForces();

    // Interface to Gromacs
    /**
    * @brief Stores a function pointer to the Gromacs force computation function
    *
    * Called via MIST_SetForceCallback, this stores a function pointer and associated
    * data, which is used by UpdateForces to make a callback to Gromacs to compute
    * updated forces and energies
    */
    void SetForceCallback(void (*f)(force_params_t *, force_arrays_t *),
                          force_params_t *p, force_arrays_t *fc);

  protected:
    void (*callback)(force_params_t *, force_arrays_t *); /**< @brief Function pointer to compute new forces */
    force_params_t *force_p; /**< @brief Parameters passed to the callback function */
    force_arrays_t *force_c; /**< @brief Pointers to arrays of partial forces */
};

#endif
