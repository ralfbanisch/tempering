/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file params.c
* @brief Implementation of parameter files
*
* The parameters functionality reads in and parses user defined parameters 
* which describe the state of the system. Helper methods are provided to 
* retrieve key value pairs in specific formats. 
*
* Original code by Nick Brown     
*
*/

#define TEMP_LINE_SIZE 100 /**< @brief Size of buffer used for reading file, effective param file line length limit  */
#define ARRAY_SIZE_STEP 10 /**< @brief Internal 'chunk' size when processing array data */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "params.h"

// Internal functions
static Param *readLine(char *);
static char *parseValue(char *);
static char *trimwhitespace(char *);
static char *getArrayName(char *);
static int getArrayIndex(char *rawValue);
static boolean toBoolean(char *);

/**
 * Given a filename this function will open that file, read in the contents and
 * then parse it into the parameters
 * format
 */
int readParams(const char *filename, Param **root)
{
    FILE *icFile;
    char *tempLine = malloc(TEMP_LINE_SIZE);
    char *readRtn = tempLine;

    Param *current = NULL;

    *root = (Param *)malloc(sizeof(Param)); // Allocate memory
    (*root)->key = NULL;
    (*root)->value = NULL;
    (*root)->next = NULL;
    (*root)->prev = NULL;

    icFile = fopen(filename, "r");
    if (!icFile)
        return ERR_NOFILE;

    while (readRtn != NULL)
    {
        readRtn = fgets(tempLine, TEMP_LINE_SIZE, icFile);
        if (readRtn != NULL)
        {
            Param *line = readLine(tempLine);

            if (line != NULL)
            {
                line->prev = current;
                if ((*root)->key == NULL)
                    *root = line;

                if (current != NULL)
                    current->next = line;

                current = line;
            }
        }
    }
    fclose(icFile);
    free(tempLine);

    return ERR_OK;
}

/**
 * Converst the elements of the array package into a set of booleans
 */
boolean *getArrayBooleanValues(ParamArrayPackage *arrayPackage)
{
    int i;
    boolean *data = (boolean *)malloc(sizeof(boolean) * arrayPackage->size);
    for (i = 0; i < arrayPackage->size; i++)
    {
        data[i] = toBoolean(arrayPackage->data[i]);
    }
    return data;
}

/**
 * Converts the elements of the array package into a set of integers
 */
int *getArrayIntValues(ParamArrayPackage *arrayPackage)
{
    int i;
    int *data = (int *)malloc(sizeof(int) * arrayPackage->size);
    for (i = 0; i < arrayPackage->size; i++)
    {
        data[i] = atoi(arrayPackage->data[i]);
    }
    return data;
}

/**
 * Converts the elements of the array package into a set of doubles
 */
double *getArrayDoubleValues(ParamArrayPackage *arrayPackage)
{
    int i;
    double *data = (double *)malloc(sizeof(double) * arrayPackage->size);
    for (i = 0; i < arrayPackage->size; i++)
    {
        data[i] = atof(arrayPackage->data[i]);
    }
    return data;
}

/**
 * Will parse the Param  elements into an array package with all items in that
 * are held within the array
 * denoted by the name key.
 */
ParamArrayPackage *getParamArray(Param *root, const char *key)
{
    ParamArrayPackage *arrayPackage =
        (ParamArrayPackage *)malloc(sizeof(ParamArrayPackage));
    Param *current = root;
    int i, c = 0, s = ARRAY_SIZE_STEP;
    // Allocate a certain size initially and increase this if the array is too
    // large
    Param **arrayConstuients = (Param **)malloc(sizeof(Param *) * s);

    while (current != NULL)
    {
        // First we gather up the nodes which form part of our array
        char *arrayName = getArrayName(current->key);
        if (arrayName != NULL && strcmp(arrayName, key) == 0)
        {
            if (c >= s)
            {
                s += ARRAY_SIZE_STEP;
                arrayConstuients =
                    (Param **)realloc(arrayConstuients, sizeof(Param *) * s);
            }
            arrayConstuients[c++] = current;
        }
        if (arrayName != NULL)
            free(arrayName);
        current = current->next;
    }

    arrayPackage->size = c;
    arrayPackage->data = (char **)malloc(sizeof(char *) * c);
    for (i = 0; i < c; i++)
    {
        // Now we actually populate the array with the user defined index being
        // the location
        Param *arrayElement = arrayConstuients[i];
        int targetIndex = getArrayIndex(arrayElement->key);
        arrayPackage->data[targetIndex] =
            (char *)malloc(strlen(arrayElement->value));
        strcpy(arrayPackage->data[targetIndex], arrayElement->value);
    }
    free(arrayConstuients);
    return arrayPackage;
}

/**
 * Given the parameters and the specific key we are searching for this method
 * iterates through and
 * will return the associated value string if it is found. If nothing can be
 * found then NULL is returned. Note
 * that this is O(n) and it might be better to implement via a hash map type
 * thing in the future.
 */
int getParamStringValue(Param *root, const char *key, char **result)
{
    Param *current = root;
    *result = NULL;

    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            *result = current->value;
            return ERR_OK;
        }
        current = current->next;
    }
    return ERR_INPUTVALUE;
}

/**
 * Given the parameter root and the key will search for the boolean value and
 * return TRUE iff the lowercase
 * value of "true" is found, anything else including no value will result in
 * false, if no key is found returns unchanged result;
 */
int getParamBooleanValue(Param *root, const char *key, boolean *result)
{
    Param *current = root;
    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            *result = toBoolean(current->value);
            return ERR_OK;
        }
        current = current->next;
    }
    return ERR_INPUTVALUE;
}

/**
 * Given the parameter root and the key will search for the double value and
 * return it if found or the default value otherwise
 */
int getParamDoubleValue(Param *root, const char *key, double *returnValue)
{
    Param *current = root;
    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            *returnValue = atof(current->value);
            return ERR_OK;
        }
        current = current->next;
    }
    return ERR_INPUTVALUE;
}

/**
 * Given the parameters and the specific key we are searching for this method
 * iterates through, locates
 * the associated value and then returns the integer representation of this. If
 * no such value is found then
 * the unchanged result is returned.
 */
int getParamIntValue(Param *root, const char *key, int *result)
{
    Param *current = root;
    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            *result = atoi(current->value);
            return ERR_OK;
        }
        current = current->next;
    }
    return ERR_INPUTVALUE;
}

/**
 * Converts a string value to a boolean
 */
static boolean toBoolean(char *value)
{
    if (strcmp("true", value) == 0)
        return TRUE;
    return FALSE;
}

/**
 * Given a parameter key=value line will parse this into the associated
 * parameter  object
 */
static Param *readLine(char *l)
{
    Param *line = (Param *)malloc(sizeof(Param));
    char *pivot = strchr(l, '=');
    if (pivot == NULL)
    {
        pivot = strchr(l, ' ');
    }
    if (pivot == NULL)
    {
        return NULL;
    }

    int lengthOfKey = pivot - l;
    int lengthOfValue = strlen(l) - (lengthOfKey + 1);

    char *key = malloc(lengthOfKey + 1);
    char *value = malloc(lengthOfValue + 1);

    strncpy(key, l, lengthOfKey);
    key[lengthOfKey] = '\0';
    strncpy(value, pivot + 1, lengthOfValue);
    value[lengthOfValue] = '\0';

    line->key = key;
    line->value = parseValue(value);
    line->prev = NULL;
    line->next = NULL;

    return line;
}

/**
 * Given a raw value will trim this and remove any comments to return just the
 * specific value
 */
static char *parseValue(char *rawValue)
{
    char *value = rawValue;
    char *pivot = strchr(rawValue, '#');
    if (pivot != NULL)
    {
        int length = pivot - rawValue;
        value = malloc(length + 1);
        strncpy(value, rawValue, length);
        free(rawValue); // New memory allocated so trash this
        value[length] = '\0';
    }

    return trimwhitespace(value);
}

/**
 * Given the key will extract out the array index or return -1 if no such can be
 * found
 */
int getArrayIndex(char *rawValue)
{
    char *value = rawValue;
    char *openBrace = strchr(rawValue, '[');
    if (openBrace != NULL)
    {
        char *closeBrace = strchr(rawValue, ']');
        if (closeBrace != NULL)
        {
            int length = (closeBrace - openBrace) -
                         1; // Minus one as we ignore open brace
            value = malloc(length + 1);
            strncpy(value, openBrace + sizeof(char), length);
            value[length] = '\0';
            int index = atoi(value);
            free(value);
            return index;
        }
    }
    return -1;
}

/**
 * Given the key will extract out the key name (ignoring the array index)
 */
static char *getArrayName(char *rawValue)
{
    char *value = NULL;
    char *pivot = strchr(rawValue, '[');
    if (pivot != NULL)
    {
        int length = pivot - rawValue;
        value = malloc(length + 1);
        strncpy(value, rawValue, length);
        value[length] = '\0';
    }
    return value;
}

/**
 * Trims leading and trailing whitespace from a string
 */
static char *trimwhitespace(char *str)
{
    char *end;
    char *current = str;

    // Trim leading space
    while (isspace(*current))
        current++;

    if (*current == 0) // All spaces?
        return str;

    // Trim trailing space
    end = current + strlen(current) - 1;
    while (end > current && isspace(*end))
        end--;

    // Write new null terminator
    *(end + 1) = 0;

    // Copy substring back to original string
    int i;
    for (i = 0; i <= strlen(current); i++)
    {
        str[i] = current[i]; 
    }

    return str;
}

/**
 * Frees all parameters allocated previously in readParams()
 */
int freeParams(Param *p)
{

    if (p->prev != NULL)
    {
        return ERR_FREE; // Should start at start of pointer
    }
    if (p->key != NULL)
        free(p->key);
    if (p->value != NULL)
        free(p->value);

    if (p->next == NULL)
        return ERR_OK;
    Param *n = p->next;
    free(p);
    n->prev = NULL;
    return freeParams(n);
}
