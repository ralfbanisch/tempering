/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Yoshida4Integrator.h
* @brief Declaration of a Yoshida order-4 Integrator class
*/

#ifndef MIST_YOSHIDA4INTEGRATOR_H
#define MIST_YOSHIDA4INTEGRATOR_H

#include "Integrator.h"

class Yoshida4Integrator : public Integrator
{
  public:
    Yoshida4Integrator(Param *p);
    virtual ~Yoshida4Integrator();

    void Step(double dt);
};

#endif
