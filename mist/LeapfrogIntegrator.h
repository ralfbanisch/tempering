/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file LeapfrogIntegrator.h
* @brief Declaration of a Leapfrog Integrator class.
*/

#ifndef MIST_LEAPFROGINTEGRATOR_H
#define MIST_LEAPFROGINTEGRATOR_H

#include "Integrator.h"

/**
* @brief Declaration of the Leapfrog Integrator class
*
* A very simple integrator class which uses the Verlet/Leapfrog algorithm.
*
* Mainly useful as a reference for the development of other integrators, and
* for comparison with native Verlet integrators in host application codes.
*
*/
class LeapfrogIntegrator : public Integrator
{
  public:
    /**
    * @brief Default Constructor
    *
    * After superclass constructor, simply prints a message.
    *
    * @param p a pointer to the parameters
    */
    LeapfrogIntegrator(Param *p);

    /**
    * @brief Default Destructor
    *
    * Does nothing
    */
    virtual ~LeapfrogIntegrator();

    /**
    * @brief Integrate positions and velocities over time dt using the 
    * Verlet Leapfrog algorithm
    *
    * Computes a full-step update of positions, updates forces, then
    * a full step update of velocities.
    *
    * @param dt the timestep in internal units
    */
    void Step(double dt);
};

#endif
