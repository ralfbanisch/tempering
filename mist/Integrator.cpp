/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Integrator.cpp
* @brief Implementation of general integrator base class, contains functionality
* that may be used by any subclass
*/

#include "Integrator.h"
#include <stddef.h>
#include <iostream>

/**
* Creates the integrator, storing the provided pointer to the user's
* parameters.
*/
Integrator::Integrator(Param *p)
{
    params = p;
    features = MIST_FEATURE_NONE;
}

/**
* Frees the integrator parameters
*/
Integrator::~Integrator()
{
    int err = freeParams(params);
    if (err == ERR_FREE)
    {
        std::cout << "MIST: Problem freeing parameters. " << std::endl;
    }
    else if (err == ERR_OK)
    {
        std::cout << "MIST: Parameters freed ok. " << std::endl;
    }
    else
    {
        std::cout << "MIST: Unknown error freeing parameters. " << std::endl;
    }

    if (system != NULL)
    {
        delete system;
        system = NULL;
    }
}

/**
* An Integrator requires access to a System class that contains the particle 
* data that is to be integrated.  This is set shortly after the Integrator
* is constructed
*/
void Integrator::SetSystem(System *s)
{
    if (s == NULL)
    {
        std::cout << "MIST: Error, no system was defined.  Build configuration "
                     "error? " << std::endl;
    }
    system = s;
}

/**
* Provides access to the System (sub)class associated with the integrator
*/
System *Integrator::GetSystem() const { return system; }

/**
* A convience function to be used by subclasses.  PositionStep updates all
* particle positions as x(i) = x(i) + v(i) * dt.
*/
void Integrator::PositionStep(double dt)
{
    Vector3 v, p;
    System *s = system;
    int n = s->GetNumParticles();
#pragma omp parallel for default(none) private(v, p) shared(dt, s, n)

    for (int i = 0; i < n; i++)
    {
        v = system->GetVelocity(i);
        p = system->GetPosition(i);
        p.x += dt * v.x;
        p.y += dt * v.y;
        p.z += dt * v.z;
        system->SetPosition(i, p);
    }
}

/**
* A convience function to be used by subclasses.  VelocityStep updates all
* particle velocities as v(i) = v(i) + F(i) / m(i) * dt.
*/
void Integrator::VelocityStep(double dt)
{
    Vector3 v, f;
    double one_over_m;
    System *s = system;
    int n = s->GetNumParticles();

#pragma omp parallel for default(none) private(v, f, one_over_m) shared(s, dt, \
                                                                        n)
    for (int i = 0; i < n; i++)
    {
        one_over_m = s->GetInverseMass(i);
        v = s->GetVelocity(i);
        f = s->GetForce(i);
        v.x += dt * one_over_m * f.x;
        v.y += dt * one_over_m * f.y;
        v.z += dt * one_over_m * f.z;
        s->SetVelocity(i, v);
    }
}

/**
* If an integrator requires any specific features from the host application code
* they will be returned by this function, allowing the host to adapt its
* behaviour accordingly.
*
* @see FEATURE_FLAGS
*/
int Integrator::GetFeatures() const { return features; }
