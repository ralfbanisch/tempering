/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file Yoshida8Integrator.h
* @brief Declaration of a Yoshida order-8 Integrator class
*/

#ifndef MIST_YOSHIDA8INTEGRATOR_H
#define MIST_YOSHIDA8INTEGRATOR_H

#include "Integrator.h"

class Yoshida8Integrator : public Integrator
{
  public:
    Yoshida8Integrator(Param *p);
    virtual ~Yoshida8Integrator();

    void Step(double dt);
};

#endif
