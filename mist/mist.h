/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file mist.h
* @brief MIST library interface definition
*
* This is the collection of function calls that may be added to a MD code in
* order for it to use MIST for integration
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @defgroup FEATURE_FLAGS MIST features flags
*
* @brief combinations of these flags may be returned by MIST_GetFeatures()
*
* MIST Integrators may require certain features from the host application, which
* might be expensive or difficult to implement.  Codes may call
*MIST_GetFeatures()
* to determine which features are required by the selected integrator, and
* either turn in those features in the code, or exit if they are not
*implemented.
* @{
*/
/** @brief MIST requires no special features */
#define MIST_FEATURE_NONE 0
/** @brief MIST requires access to components of the force-field */
#define MIST_FEATURE_FORCE_COMPONENTS 1
/** @} */

/**
* @defgroup MIST_API MIST Library API
*
* @brief The MIST library API
*
* A C interface to MIST which may be called by host MD applications to make use
*of MIST integrators.
* @{
*/

/**
* @brief Initialises the MIST library
*
* This is the first function that must be called by the host application, MIST
*will read in the users's mist.params file and set itself up accordingly.
*
* @return zero on success, non-zero otherwise
*/
int MIST_Init();

/**
* @brief Sets the number of particles to be integrated
*
* This tells the MIST library how many particles exist in the host application,
*and therefore how many entries in the position, velocitie, force arrays to
*expect.  The number of particles is assumed to be fixed for the duration of the
*simulation.
*
* @param n the number of particles
* @return zero on success, non-zero otherwise
*/
int MIST_SetNumParticles(int n);

/**
* @brief Passes MIST a pointer to the particle positions
*
* MIST needs to be able to read and modify the particle positions.  This
*function takes a pointer to position data in the host application and stores it
*for later use in MIST.  This is usually an array of 3-vectors, but may be a
*more complex data structure.  If the location of the data changes for any
*reason, you must call MIST_SetPositions() again.
*
* @param positions pointer to the position data
* @return zero on success, non-zero otherwise
*/
int MIST_SetPositions(void *positions);

/**
* @brief Passes MIST a pointer to the particle velocities
*
* MIST needs to be able to read and modify the particle velocities.  This
*function takes a pointer to velocity data in the host application and stores it
*for later use in MIST.  This is usually an array of 3-vectors, but may be a
*more complex data structure.  If the location of the data changes for any
*reason, you must call MIST_SetVelocities() again.
*
* @param velocities pointer to the velocity data
* @return zero on success, non-zero otherwise
*/
int MIST_SetVelocities(void *velocities);

/**
* @brief Passes MIST a pointer to the particle masses
*
* MIST needs to be able to read the particle masses.  This function takes a
*pointer to mass data in the host application and stores it for later use in
*MIST.  This may be for example an array of masses, an array of inverse masses,
*or a more complex particle data structure.  If the location of the data changes
*for any reason, you must call MIST_SetMasses() again.
*
* @param masses pointer to the mass data
* @return zero on success, non-zero otherwise
*/
int MIST_SetMasses(void *masses);

/**
* @brief Passes MIST a pointer to the particle kind labels
*
* MIST needs to be able to read the particle kind labels, typically short
*strings containing the element labels (H, C, N ...).  This may be presented as
*an array of strings or as part of a more complex particle data structure.  If
*the location of the data changes for any reason, you must call MIST_SetKinds()
*again.
*
* @param kinds pointer to the kind data
* @return zero on success, non-zero otherwise
*/
int MIST_SetKinds(void *kinds);

/**
* @brief Passes MIST a pointer to the potential energies
*
* MIST needs to be able to read the potential energy computed by the host
*application for a given configuration of particles.  As well as the total
*potential energy, MIST must also be able to access particular components of the
*force-field - Bond, Angle, Dihedral etc.. The pointer passed to MIST here must
*allow access to each of these components.  If the location of the data changes,
*you must call MIST_SetPotentialEnergy() again.
*
* @param potential_energy pointer to the potential energy data
* @return zero on success, non-zero otherwise
*/
int MIST_SetPotentialEnergy(void *potential_energy);

/**
* @brief Passes MIST a pointer to the forces
*
* MIST needs to be able to read the forces computed by the host application for
*a given configuration of particles.  As well as the total force on each
*particle, MIST must also be able to access particular components of the
*force-field - Bond, Angle, Dihedral etc. The pointer passed to MIST here must
*allow access to each of these components.  If the location of the data changes,
*you must call MIST_SetForces() again.
*
* @param forces pointer to the force data
* @return zero on success, non-zero otherwise
*/
int MIST_SetForces(void *forces);

/**
* @brief Returns the application features required by MIST
*
* To determine what functionality is needed by the MIST library to perform
*integration, host application codes may call MIST_GetFeatures().  This will
*return a logical-OR'ed combination of the MIST feature flags.
* @sa FEATURE_FLAGS
*
* @return logical-OR'ed combination of the MIST feature flags
*/
int MIST_GetFeatures();

/**
* @brief Makes a single MD step using MIST
*
* A call to MIST_Step() hands over control of execution from the host
*appilcation to the MIST library to perform a single MD step of duration dt.
*The call will return when the step has been completed.  MIST may make callbacks
*during the step to update forces.
*
* @param dt MD time-step (in host application's units)
* @return zero on success, non-zero otherwise*/
int MIST_Step(double dt);

/**
* @brief Closes down the MIST library, ready for exit
*
* Once the MD run has been completed, the MIST library should be shut down by
*calling MIST_Cleanup(), to properly deallocate internal state variables ready
*for application exit
*
* @return zero on success, non-zero otherwise
*/
int MIST_Cleanup();

// Code-specific callback registration functions
#ifdef __MIST_WITH_NAMD_LITE
#include "step/step_defn.h"
/**
* @brief Passes MIST a function pointer from NAMD-Lite used to compute new
*forces
*
* MIST modifies particle positions and velocities during the call to
*MIST_Step().  To get updated forces and potential energies, MIST must be able
*to make a callback to the host application to execute the force calculation.
*MIST_SetForceCallback registers a function pointer and application data which
*will be used to execute the callback.
*
* @param callback a pointer to the host application function used to calculate
*forces
* @param s the Step structure from NAMD-Lite holding the required state to
*compute forces
* @return zero on success, non-zero otherwise
*/
int MIST_SetForceCallback(int (*callback)(Step *), Step *s);
#endif

#ifdef __MIST_WITH_GROMACS
#include "gromacs/legacyheaders/types/mist_forcecallback.h"
/**
* @brief Passes MIST a function pointer from Gromacs used to compute new forces
*
* MIST modifies particle positions and velocities during the call to
*MIST_Step().  To get updated forces and potential energies, MIST must be able
*to make a callback to the host application to execute the force calculation.
*MIST_SetForceCallback registers a function pointer and application data which
*will be used to execute the callback.
*
* @param callback a pointer to the host application function used to calculate
*forces
* @param p the force_params_t structure from Gromacs containing all the force
*evaluation parameters
* @param fc the force_arrays_t structure containing arrays of forces from
*different components of the force-field
* @return zero on success, non-zero otherwise
*/

int MIST_SetForceCallback(void (*callback)(force_params_t *, force_arrays_t *),
                          force_params_t *p, force_arrays_t *fc);
#endif

/** @} */

#ifdef __cplusplus
}
#endif
