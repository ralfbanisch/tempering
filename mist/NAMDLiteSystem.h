/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file NAMDLiteSystem.h
* @brief Declaration of specific system class for NAMD-Lite
*/

#ifndef MIST_NAMD_LITE_SYSTEM_H
#define MIST_NAMD_LITE_SYSTEM_H

#include "System.h"
#include "step/step_defn.h"

/**
* @brief A System class which provides accessors for particle data inside the NAMD-Lite (MDX) code
*
* This is a specialisation of the System interface which is specific to the NAMD-Lite code.  It provides access to an Integrator to the particle data stored in various arrays within NAMD-Lite, and also the ability to compute updates forces, calling NAMD-Lite's force evaluation function by means of a callback.
*
*/
class NAMDLiteSystem : public System
{
  public:
    /**
    * @brief Default Constructor
    *
    * Sets the NAMD-Lite-specific unit conversion factors:
    * - Energy in kJ/mol
    * - Time in ps
    * - Mass in AMU
    * - Length in Angstrom
    *
    */
    NAMDLiteSystem();

    /**
    * @brief Default Destructor
    *
    * Does nothing
    */
    ~NAMDLiteSystem();

    // Interface to Integrators
    Vector3 GetVelocity(int i) const;
    void SetVelocity(int i, Vector3 v);

    Vector3 GetPosition(int i) const;
    void SetPosition(int i, Vector3 p);

    Vector3 GetForce(int i) const;
    Vector3 GetForce(int i, force_component_t t) const;
    double GetMass(int i) const;
    double GetInverseMass(int i) const;
    double GetPotentialEnergy() const;
    double GetPotentialEnergy(force_component_t t) const;

    void UpdateForces();
    char *GetKind(int i) const;

    // Interface to NAMD-Lite
    /**
    * @brief Stores a function pointer to the NAMD-Lite force computation function
    *
    * Called via MIST_SetForceCallback, this stores a function pointer and associated
    * data (a Step object), which is used by UpdateForces to make a callback to 
    * NAMD-Lite to compute updated forces and energies
    */
    void SetForceCallback(int (*f)(Step *), Step *s);

  protected:
    int (*callback)(Step *); /**< @brief Function pointer to compute new forces */
    Step *step; /**< @brief Data passed to the callback function */
};

#endif
