/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file System.h
* @brief Declaration of System abstract base class.
*/

#ifndef MIST_SYSTEM_H
#define MIST_SYSTEM_H

#include "Vector3.h"

/**
* @brief Declaration of a System abstract base class, describing the particles
*which are to be integrated.
*
* This is the API that Integrators use to access and modify the state of the
*system.  It provides accessors for the system state, unit conversion routines.
*The API is implemented by a particular sub-class, specific to a host
*application.
*
* Accessors are also provided to set up the system - these are not called
*directly but are used to implement the MIST library API, which is called by the
*host application.
*/
class System
{
  public:
    /**
    * @brief Selects components of the forcefield
    *
    * MIST can separate the contributions to the total potential energy / force
    *on a particle
    * returned by GetForce() / GetPotentialEnergy() into several components.
    *This enum selects which component to return.
    */
    enum force_component_t
    {
        MIST_FORCE_ALL,         /**< The complete forcefield */
        MIST_FORCE_BOND,        /**< Bonded terms only */
        MIST_FORCE_BEND,        /**< Angle terms only */
        MIST_FORCE_PROPDIHED,   /**< Proper dihedral terms only */
        MIST_FORCE_IMPROPDIHED, /**< Improper dihedral terms only */
        MIST_FORCE_NONBONDED    /**< Non-bonded terms only */
    };

    /**
    * @brief Default Constructor
    *
    * Does nothing
    */
    System();

    /**
    * @brief Default Destructor
    *
    * Does nothing
    */
    virtual ~System();

    /**
    * @defgroup INTEGRATOR_INTERFACE System API
    *
    * @brief API for Integrators to interact with the host application code.
    *
    * @{
    */

    /**
    * @brief Getter for the number of particles
    *
    * Gets the number of particles, useful as a loop bound.
    *
    * @return the number of particles in the System
    */
    int GetNumParticles() const;

    /**
    * @brief Getter for particle velocities
    *
    * Gets a Vector3 containing the velocity of particle i.
    *
    * @param i the particle index
    * @return the velocity of particle i
    */
    virtual Vector3 GetVelocity(int i) const = 0;

    /**
    * @brief Setter for particle velocities
    *
    * Sets the velocity of particle i.
    *
    * @param i the particle index
    * @param v the new velocity
    */
    virtual void SetVelocity(int i, Vector3 v) = 0;

    /**
    * @brief Getter for particle positions
    *
    * Gets a Vector3 containing the position of particle i.
    *
    * @param i the particle index
    * @return the position of particle i
    */
    virtual Vector3 GetPosition(int i) const = 0;

    /**
    * @brief Setter for particle positions
    *
    * Sets the position of particle i.
    *
    * @param i the particle index
    * @param p the new position
    */
    virtual void SetPosition(int i, Vector3 p) = 0;

    /**
    * @brief Getter for forces
    *
    * Returns the total force on particle i.
    *
    * @param i the particle index
    * @return the total force on particle i
    */
    virtual Vector3 GetForce(int i) const = 0;

    /**
     * @brief Getter for force components
     *
     * Returns a component of the force on particle i.
     * \see force_component_t
     *
     * @param i the particle index
     * @param t which component of the force-field to select
     * @return the force on particle i from the selected component of the
     *force-field
     */
    virtual Vector3 GetForce(int i, force_component_t t) const = 0;

    /**
    * @brief Getter for particle masses
    *
    * Gets the mass of particle i.
    *
    * @param i the particle index
    * @return the mass of particle i
    */
    virtual double GetMass(int i) const = 0;

    /**
    * @brief Getter for particle inverse masses
    *
    * Gets one over the mass of particle i.
    * Usually more efficient to call this rather than 1/GetMass() as
    * the inverse mass may be precomputed and stored.
    *
    * @param i the particle index
    * @return 1/mass of particle i
    */
    virtual double GetInverseMass(int i) const = 0;

    /**
    * @brief Getter for potential energy
    *
    * Returns the total potential energy
    *
    * @return the potential energy of the system
    */
    virtual double GetPotentialEnergy() const = 0;

    /**
     * @brief Getter for potential energy components
     *
     * Returns the potential energy due to the selected component of the
     *force-field
     * \see force_component_t
     *
     * @param t which component of the force-field to select
     * @return the potential energy from the selected component of the
     *force-field
     */
    virtual double GetPotentialEnergy(force_component_t t) const = 0;

    /**
    * @brief Getter for particle kind labels
    *
    * Gets a string representing the kind (species) of particle i.
    * This is usually a label taken from the input geometry (a PDB file or
    *similar), but depends on the host application code.
    *
    * @param i the particle index
    * @return a null-terminated string containing the kind label
    */
    virtual char *GetKind(int i) const = 0;

    /**
    * @brief Update the forces on the system given the current particle
    *positions
    *
    * After modifying particle positions, an Integrator may call UpdateForces().
    * This makes a callback to the host application's force evaluation routine
    * and computes up-to-date forces and potential energies, which may then be
    * read by calling GetForce() / GetPotentialEnergy()
    */
    virtual void UpdateForces() = 0;

    /**
    * @brief Get Boltzmann's constant in internal units
    *
    * MIST Integrators work in whatever internal units are used by the
    * host application code.  To convert input parameters from a specified
    * unit to internal units, this function returns the value of Boltzmann's
    * constant (energy / temperature).
    *
    * @return Boltzmann's constant in internal units
    */
    double Boltzmann() const;

    /**
    * @brief Get one picosecond in internal units
    *
    * MIST Integrators work in whatever internal units are used by the
    * host application code.  To convert input parameters from a specified
    * unit to internal units, this function returns one picosecond (time).
    *
    * @return one picosecond in internal units
    */
    double Picosecond() const;

    /**
    * @brief Get the unit mass (AMU) in internal units
    *
    * MIST Integrators work in whatever internal units are used by the
    * host application code.  To convert input parameters from a specified
    * unit to internal units, this function returns the unit mass (1 AMU)
    * (mass).
    *
    * @return the unit mass in internal units
    */
    double UnitMass() const;

    /**
    * @brief Get one Angstrom in internal units
    *
    * MIST Integrators work in whatever internal units are used by the
    * host application code.  To convert input parameters from a specified
    * unit to internal units, this function returns one Angstrom (length).
    *
    * @return one Angstrom in internal units
    */
    double Angstrom() const;

    /**
    * @}
    */

    // *************************

    /**
    * @brief Setter for number of particles
    *
    * Sets the number of particles to be integrated.  This is used is an upper
    * bound for indexing into the velocity, position, ... data.
    *
    * @param n the number of particles
    */
    void SetNumParticles(int n);

    /**
    * @brief Setter for the velocity pointer
    *
    * Stores a pointer to the particle velocity data.
    *
    * @param v pointer to velocity data
    */
    void SetVelocityPtr(void *v);

    /**
    * @brief Setter for the position pointer
    *
    * Stores a pointer to the particle position data.
    *
    * @param p pointer to position data
    */
    void SetPositionPtr(void *p);

    /**
    * @brief Setter for the mass pointer
    *
    * Stores a pointer to the particle mass data.
    *
    * @param m pointer to mass data
    */
    void SetMassPtr(void *m);

    /**
    * @brief Setter for the potential energy pointer
    *
    * Stores a pointer to the potential energy data.
    *
    * @param pe pointer to potential energy data
    */
    void SetPotentialEnergyPtr(void *pe);

    /**
    * @brief Setter for the force pointer
    *
    * Stores a pointer to the particle force data.
    *
    * @param f pointer to particle force data
    */
    void SetForcePtr(void *f);

    /**
    * @brief Setter for the kind pointer
    *
    * Stores a pointer to the particle kind (species) data.
    *
    * @param t pointer to kind data
    */
    void SetKindsPtr(void *k);

    // *************************

    // Base class variables
  protected:
    int numParticles; /**< @brief The number of particles to be integrated */
    void *velocities; /**< @brief The particle velocity pointer */
    void *positions;  /**< @brief The particle position pointer */
    void *masses;     /**< @brief The particle mass pointer */
    void *potential_energy; /**< @brief The potential energy pointer */
    void *forces;           /**< @brief The particle force pointer */
    void *kinds;            /**< @brief The particle kind pointer */

    // Conversion factors for each code's units system.
    double boltzmann;  /**< @brief Boltzmann's constant in internal units */
    double picosecond; /**< @brief 1 picosecond in internal units */
    double unitmass;   /**< @brief 1 atomic mass unit (AMU) in internal units */
    double angstrom;   /**< @brief 1 angstrom in internal units */
};

#endif
