/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file System.cpp
* @brief Implementation of the System base class, accessors only implemented
*/

#include "System.h"
#include <stddef.h>

System::System() {}

System::~System() {}

int System::GetNumParticles() const { return numParticles; }

void System::SetNumParticles(int n) { numParticles = n; }

void System::SetVelocityPtr(void *v) { velocities = v; }

void System::SetPositionPtr(void *p) { positions = p; }

void System::SetMassPtr(void *m) { masses = m; }

void System::SetPotentialEnergyPtr(void *pe) { potential_energy = pe; }

void System::SetForcePtr(void *f) { forces = f; }

void System::SetKindsPtr(void *k) { kinds = k; }

double System::Boltzmann() const { return boltzmann; }

double System::Picosecond() const { return picosecond; }

double System::UnitMass() const { return unitmass; }

double System::Angstrom() const { return angstrom; }
