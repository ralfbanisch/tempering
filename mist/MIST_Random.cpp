/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2015, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS.txt                      *
*                                                                              *
*            MIST is free software.  See LICENSE.txt for details               *
*                                                                              *
*******************************************************************************/

/**
* @file MIST_Random.cpp
* @brief Random number generator class.  Originally from NAMD Random.h (see
*below).
*
* Copyright (c) 1993 Martin Birgmeier
*
* All rights reserved.
*
* You may redistribute unmodified or modified versions of this source
* code provided that the above copyright notice and this and the
* following conditions are retained.
*
* This software is provided ``as is'', and comes with no warranties
* of any kind. I shall in no event be liable for anything that happens
* to anyone/anything when using this software.
*/

#include <math.h>
#include "MIST_Random.h"
#include <iostream>

#ifdef _MSC_VER
#define INT64_LITERAL(X) X##i64
#else
#define INT64_LITERAL(X) X##LL
#endif

#define RAND48_SEED INT64_LITERAL(0x00001234abcd330e)
#define RAND48_MULT INT64_LITERAL(0x00000005deece66d)
#define RAND48_ADD INT64_LITERAL(0x000000000000000b)
#define RAND48_MASK INT64_LITERAL(0x0000ffffffffffff)

MIST_Random::MIST_Random()
/* default constructor */
{
    init(0);
    rand48_seed = RAND48_SEED;
}

/* constructor with seed */
MIST_Random::MIST_Random(unsigned long seed) { init(seed); }

void MIST_Random::init(unsigned long seed)
{
    second_gaussian = 0;
    second_gaussian_waiting = 0;
    rand48_seed = seed & INT64_LITERAL(0x00000000ffffffff);
    rand48_seed = rand48_seed << 16;
    rand48_seed |= RAND48_SEED & INT64_LITERAL(0x0000ffff);
    rand48_mult = RAND48_MULT;
    rand48_add = RAND48_ADD;
}

void MIST_Random::skip()
{
    rand48_seed = (rand48_seed * rand48_mult + rand48_add) & RAND48_MASK;
}

/* split into numStreams different steams and take stream iStream */
void MIST_Random::random_split(int iStream, int numStreams)
{
    int64_t save_seed;
    int64_t new_add;
    int i;

    /* make sure that numStreams is odd to ensure maximum period */
    numStreams |= 1;

    /* iterate to get to the correct stream */
    for (i = 0; i < iStream; ++i)
        skip();

    /* save seed and add so we can use skip() for our calculations */
    save_seed = rand48_seed;

    /* calculate c *= ( 1 + a + ... + a^(numStreams-1) ) */
    rand48_seed = rand48_add;
    for (i = 1; i < numStreams; ++i)
        skip();
    new_add = rand48_seed;

    /* calculate a = a^numStreams */
    rand48_seed = rand48_mult;
    rand48_add = 0;
    for (i = 1; i < numStreams; ++i)
        skip();
    rand48_mult = rand48_seed;

    rand48_add = new_add;
    rand48_seed = save_seed;

    second_gaussian = 0;
    second_gaussian_waiting = 0;
}

/* return a number uniformly distributed between 0 and 1 */
double MIST_Random::random_uniform()
{
    const double exp48 = (1.0 / (double)(INT64_LITERAL(1) << 48));

    skip();
    return ((double)rand48_seed * exp48);
}

/* return a number from a standard gaussian distribution */
double MIST_Random::random_gaussian()
{
    double fac, rsq, v1, v2;

    if (second_gaussian_waiting)
    {
        second_gaussian_waiting = 0;
        return second_gaussian;
    }
    else
    {
        /*
         * rsq >= 1.523e-8 ensures abs result < 6
         * make sure we are within unit circle
         */
        do
        {
            v1 = 2.0 * random_uniform() - 1.0;
            v2 = 2.0 * random_uniform() - 1.0;
            rsq = v1 * v1 + v2 * v2;
        } while (rsq >= 1. || rsq < 1.523e-8);
        fac = sqrt(-2.0 * log(rsq) / rsq);
        /*
         * Now make the Box-Muller transformation to get two normally
         * distributed random numbers.  Save one and return the other.
         */
        second_gaussian_waiting = 1;
        second_gaussian = v1 * fac;
        return v2 * fac;
    }
}

/* return a vector of gaussian random numbers */
Vector3 MIST_Random::random_gaussian_vector()
{
    Vector3 vec;

    vec.x = random_gaussian();
    vec.y = random_gaussian();
    vec.z = random_gaussian();
    return vec;
}

/* return a random long */
long MIST_Random::random_integer()
{
    skip();
    return ((rand48_seed >> 17) & INT64_LITERAL(0x000000007fffffff));
}
