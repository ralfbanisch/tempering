#!/bin/bash

file="../../output.txt"
echo " Running verlet test for NAMDLite..."  >> $file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
gawk '{sub(/integrator/,"&  verlet");print}' mist.params.template > mist.params

OS=$( uname )
ARCH='generic'
BUILD=$OS
BUILD+=_$ARCH

$DIR/../../builds/NAMDLite/mdx_pre2.0.3/build/$BUILD/bin/./mdsim adp.mist.config > outPut

function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "   FAILED" >&2    >> $file
	echo " " 
	exit 1
    else
        echo "   PASSED" >&2     >> $file
    fi
    return $status
}

gawk '!/build: / && !/total wall-clock time/' outPut > outPut-namdlite-verlet-test

echo '  * Testing OpenMP 1 thread namdlite with verlet integrator: '   >> ../../$file
test sdiff -s  outPut-namdlite-verlet-test outPut-namdlite-verlet  > DiffNMDOmp1Verlet

echo "  * Removing some files." >> $file
rm outPut output.adp.* &> /dev/null
echo " "  >> $file

exit 0
