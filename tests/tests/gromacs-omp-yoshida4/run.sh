#!/bin/bash

file="../../output.txt"
echo " Running yoshida4 test for Gromacs OpenMP..."  >> $file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "integrator yoshida4" > mist.params

export GMXLIB=$DIR/../../builds/gromacs-openmp/gromacs-5.0.2_omp/gromacs-5.0.2/share/top
#Run batch script instead
export OMP_NUM_THREADS=2

$DIR/../../builds/gromacs-openmp/gromacs-5.0.2_omp/gromacs-5.0.2/build/bin/./gmx grompp -f water.mdp -p water.top -c water.gro  -o mineMIST.tpr > outGrompp  2>&1
$DIR/../../builds/gromacs-openmp/gromacs-5.0.2_omp/gromacs-5.0.2/build/bin/./gmx mdrun -nt 2 -s mineMIST.tpr -mp water.top > outMdrun  2>&1

FILE="md.log"
if [[ ! -f "$FILE" ]] ; then
    echo " * File" $FILE " does not exist, something went wrong running gromacs."
    exit 1
fi ;

function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "   FAILED" >&2   >> $file
	echo " " 
	exit 1
    else
        echo "   PASSED" >&2   >> $file
    fi
    return $status
}

#Compare the energies for the last step of the test file 'file1' against the new md.log file (file2)                                                   
gawk 'BEGIN{"grep -c ^ md.log"|getline total; num=0} /Writing checkpoint, step 50 / {num=NR} END{print num} ' md.log > 2a
gawk 'BEGIN{"more 2a"|getline num} NR > num  {print }' md.log > aa
gawk '/Bond /{getline;print;}' aa > file2
gawk '/Kinetic En/{getline;print;}' aa >> file2


paste file{1,2} |gawk 'function abs(x) {return ((x<0.0) ? -x : x)} {for (i=1; i<=NF/2;i++) printf "%s ",abs((($i==$i+0)?$i-$(i+NF/2): $i) /$i); print ""}' > file3 

gawk '{ for (i=1;i<=NF;i++) if ($i>.009) print $i}' file3 > file4

FILE=file4

echo '  * Testing OpenMP 2 threads gromacs-openmp with yoshida4 integrator: ' >> $file
if [[ -s $FILE ]] ; then
#echo "$FILE has data."                                                                                                                                
test failme
else
#echo "$FILE is empty."                                                                                                                                
test
fi ;

echo "  * Removing some files." >> $file
rm traj.trr state.cpt ener.edr confout.gro state_prev.cpt 2a   &> /dev/null
echo " "  >> $file
exit 0
