#!/bin/bash

now=`date '+%F_%H-%M-%S'`
mkdir test_$now
cd test_$now
file="output.txt"

echo "All produced output can be found in: " test_$now/$file
echo "Test harness for MIST"  > $file
echo "---------------------"  >> $file

echo "Testing the availability of required tools for test suite." >> $file

g++ -v &> /dev/null


  if (( $? == '0' )); then
      echo " g++ compiler is available."  >> $file
  else
      echo " FAIL: g++ compiler not available: Exiting"   >> $file
      exit 1
  fi

gcc -v &> /dev/null


  if (( $? == '0' )); then
      echo " gcc compiler is available."  >> $file
  else
      echo " FAIL: gcc compiler not available: Exiting"  >> $file
      exit 1
  fi

gawk --v &> /dev/null


  if (( $? == '0' )); then
      echo " gawk is available."  >> $file
  else
      echo " FAIL: gawk not available: Exiting"  >> $file
      exit 1
  fi

cmake &> /dev/null


  if (( $? == '0' )); then
      echo " cmake is available." >> $file
  else
      echo " FAIL: cmake not available: Exiting"  >> $file
      exit 1
  fi

wget --version &> /dev/null

  if (( $? == '0' )); then
      echo " wget is available."   >> $file
  else
      echo " FAIL: wget is not available."  >> $file
      echo " Testing for curl instead."     >> $file
      curl  &> /dev/null
      if (( $? == '2' )); then
          echo " curl is available."        >> $file
      else
          echo " FAIL: curl is not available. Exiting."  >> $file
          exit 1
      fi
  fi



echo " " >> $file
echo "Running tests in directory test_$now"  >> $file
echo "===================================================" >> $file

cp -r ../builds .
cp -r ../tests .

# Each directory in should have a build.sh script which makes a copy of the source, builds the MIST library and MD code linked with it.
cd builds
builds=`find * -maxdepth 1 -type d`

echo "Building MD codes with MIST" >> ../$file
echo -e " Builds:" >> ../$file
echo -e "$builds" >> ../$file

buildErr=0

# Execute each of the build scripts in turn
for build in $builds
do
  echo " Building: $build ... any output from the build process can be found in " test_$now/builds/$build/$build-build.txt   >> ../$file
  cd $build
  ./build.sh
  if (( $? )); then
    echo "  FAIL: Build failed"   >> ../../$file
    buildErr=$(( buildErr + 1 ))
  fi
  cd ..
done
cd ..

# Each directory should have a run.sh script, and set of input files and reference output.  Run.sh runs MD using MIST and compares with the reference results.
cd tests
testdirs=`find * -maxdepth 1 -type d`

testErr=0

echo " " >> ../$file
echo "Executing tests..."   >> ../$file
echo -e " Tests:"           >> ../$file
echo -e "$testdirs"   >> ../$file

for test in $testdirs
do
  echo " Testing: $test ..." >> ../$file
  cd $test
  ./run.sh
  if (( $? )); then
    echo "  FAIL: Test failed" >> ../../$file
    testErr=$(( testErr + 1 ))
  fi
  cd ..
done
cd ..


echo " " >> $file
echo "Test summary:"   >> $file
echo "============="   >> $file
if [ $buildErr -eq 0 ]; then
    echo "All done, builds successful!"   >> $file
 else
    echo $buildErr "build error(s) encountered."  >> $file
fi

if [ $testErr -eq 0 ]; then
    echo "All done, tests successful!"   >> $file
else
    echo $testErr "test run error(s) encountered."  >> $file
fi
