#!/bin/bash
file="gromacs-openmp-build.txt"
upfile="../../output.txt"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Building GROMACS-OpenMP"    > $file

wget &> /dev/null
  if (( $? == '1' )); then
      echo " * Downloading gromacs-5.0.2.tar.gz. with wget."  >> $DIR/$file
      wget ftp://ftp.gromacs.org/pub/gromacs/gromacs-5.0.2.tar.gz   &> /dev/null
      if (( $? != 0)); then
          echo "   * Problem downloading gromacs-5.0.2.."   >> $DIR/$file
          exit 1
      fi
  else
      curl  &> /dev/null
      if (( $? == '2' )); then
          echo "  * Downloading gromacs-5.0.2.tar.gz. with curl."    >> $DIR/$file
          curl -k -O ftp://ftp.gromacs.org/pub/gromacs/gromacs-5.0.2.tar.gz   &> /dev/null
          if (( $? != 0)); then
              echo "   * Problem downloading gromacs-5.0.2.."   >> $DIR/$file
              exit 1
          fi
      else
          echo " FAIL: wget/curl are not available. Exiting."  >> $DIR/$file
          exit 1
      fi
  fi


echo " * Copying mist-directory to current directory:"   >> $DIR/$file
echo "    "$DIR                                          >> $DIR/$file

mkdir mist &> /dev/null
export MIST_PATH=$(pwd)/mist
cp -fr $DIR/../../../../mist/* mist/ &> /dev/null
if (( $? )); then
 echo " * Problem copying mist to " $MIST_PATH "."       >> $DIR/$file
 exit 1                                                                                                                             
fi

mkdir gromacs-5.0.2_omp &> /dev/null
#echo "Creating new directory for gromacs-5.0.2_omp"
tar -xzvf gromacs-5.0.2.tar.gz -C gromacs-5.0.2_omp > Tar.txt 2>&1

export GROMACS_PATH=$DIR/gromacs-5.0.2_omp/gromacs-5.0.2
echo " * Using GROMACS source from: $GROMACS_PATH"    >> $DIR/$file
  
echo " "     >> $DIR/$file
cd $DIR/mist
echo " * Cleaning mist" &> /dev/null    >> $DIR/$file
make -f Makefile_Gromacs clean &> /dev/null
echo " * Patching gromacs-openMP."      >> $DIR/$file
make -f Makefile_Gromacs patch &> /dev/null
if (( $? )); then
 echo "   * Problem making mist patch."   >> $DIR/$file
 exit 1
fi


cd $GROMACS_PATH
patch -p0 < $DIR/mist/patches/Gromacs-5.0.2.patch &> /dev/null
if (( $? )); then
    echo "  * Problem patching gromacs-openMP."    >> $DIR/$file
    exit 1
fi


cd - &> /dev/null
echo " * Making MIST library."    >> $DIR/$file    
make -f Makefile_Gromacs lib &> /dev/null
if (( $? )); then
 echo "   * Problem making mist library."   >> $DIR/$file
 exit 1                                                                                                                         
fi


if [[ $MIST_PATH == "" ]]; then
  echo " * MIST_PATH is not set"    >> $DIR/$file
  exit 1                                                                                                                                
fi
echo " * Using MIST source from:"    >> $DIR/$file
echo "    $MIST_PATH"                >> $DIR/$file


cd $GROMACS_PATH
if [[ -e $GROMACS_PATH/build ]]; then
 echo "Old build-directory in gromacs-5.0.2 already exists, deleting it."   >> $DIR/$file
 rm -fr build
fi
mkdir build
cd build
export FLAGS='-D__MIST_WITH_GROMACS'
export CXX=g++
export CC=gcc
export CMAKE_PREFIX_PATH=/usr/lib 
echo "Configuring gromacs-openMP"    >> $DIR/$file
cmake ../ -DGMX_MPI=OFF -DGMX_GPU=OFF -DGMX_X11=OFF -DGMX_DOUBLE=OFF -DCMAKE_C_FLAGS="$FLAGS" -DCMAKE_CXX_FLAGS="$FLAGS" -DGMX_BUILD\
_OWN_FFTW=ON -DGMX_OPENMP=ON > CMakeReport &> /dev/null
if (( $? )); then
    echo " * Problem configuring gromacs-openMP with cmake."   >> $DIR/$file
    exit 1
fi


echo " * Making gromacs-OpenMP"    >> $DIR/$file
make  &> /dev/null
if (( $? )); then
 echo " * Problem compiling gromacs-OpenMP."    >> $DIR/$file
 echo " "
 exit 1
else
 echo "Finished making gromacs-OpenMP."    >> $DIR/$file
 echo " " >> $DIR/$file
 echo "Finished making gromacs-OpenMP."    >> $DIR/$upfile
 echo " " >> $DIR/$upfile
 export GMXLIB=$DIR/../../../../gromacs-5.0.2_omp/gromacs-5.0.2/share/top
 exit 0
fi

