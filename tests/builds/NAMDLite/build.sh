#!/bin/bash

file="NAMDLite-build.txt"
upfile="../../output.txt"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Building NAMDLite"    > $DIR/$file

wget &> /dev/null
  if (( $? == '1' )); then
      echo " * Downloading mdx_pre2.0.3.tar.gz  with wget."  >> $DIR/$file
      wget http://www.ks.uiuc.edu/Development/MDTools/namdlite/files/mdx_pre2.0.3.tar.gz   &> /dev/null
      if (( $? != 0)); then
          echo "   * Problem downloading mdx_pre2.0.3.tar.gz."   >> $DIR/$file
          exit 1
      fi
  else
      curl  &> /dev/null
      if (( $? == '2' )); then
          echo "  * Downloading mdx_pre2.0.3.tar.gz with curl."    >> $DIR/$file
          curl -k -O http://www.ks.uiuc.edu/Development/MDTools/namdlite/files/mdx_pre2.0.3.tar.gz   &> /dev/null
          if (( $? != 0)); then
              echo "   * Problem downloading mdx_pre2.0.3.tar.gz."   >> $DIR/$file
              exit 1
          fi
      else
          echo " FAIL: wget/curl are not available. Exiting."  >> $DIR/$file
          exit 1
      fi
  fi


echo " * Copying mist-directory to current directory:"   >> $DIR/$file
echo "    "$DIR                                          >> $DIR/$file

mkdir mist &> /dev/null
export MIST_PATH=$(pwd)/mist
cp -fr $DIR/../../../../mist/* mist/  &> /dev/null
if (( $? )); then
 echo "  * Problem copying mist to " $MIST_PATH "."     >> $DIR/$file
 exit 1                                                                                                                             
fi


directory=$DIR/mdx_pre2.0.3/
tar -xzvf mdx_pre2.0.3.tar.gz  > Tar.txt 2>&1
export NAMD_LITE_PATH=$directory
echo " * NAMD_LITE_PATH is set to " $directory      >> $DIR/$file

echo " * Using NAMDLite source from: $NAMD_LITE_PATH"   >> $DIR/$file

echo " "                                                >> $DIR/$file
cd $DIR/mist
echo " * Cleaning mist"                                 >> $DIR/$file
make -f Makefile_NAMD-Lite clean &> /dev/null
echo " * Patching namdlite."                            >> $DIR/$file
make -f Makefile_NAMD-Lite patch &> /dev/null
if (( $? )); then
 echo "  * Problem making mist patch."                 >> $DIR/$file
 exit 1
fi

cd $NAMD_LITE_PATH
patch -p0 < $DIR/mist/patches/NAMD-Lite_2.0.3.patch &> /dev/null
if (( $? )); then
    echo "  * Problem patching NAMDLite."              >> $DIR/$file
    exit 1
fi



cd -  &> /dev/null
echo " * Making MIST library."                         >> $DIR/$file
make -f Makefile_NAMD-Lite lib &> /dev/null
if (( $? )); then
 echo "  * Problem making mist library."               >> $DIR/$file
 exit 1                                                                                                                    
fi


if [[ $MIST_PATH == "" ]]; then
  echo " * MIST_PATH is not set"                        >> $DIR/$file
  exit 1                                                                                                                                
fi
echo " * Using MIST source from:"                       >> $DIR/$file
echo "     $MIST_PATH"                                  >> $DIR/$file


cd $NAMD_LITE_PATH

echo " "                                                >> $DIR/$file
echo " * Checking if a Mac is used, if so update arch/Darwin_generic.mk."  >> $DIR/$file
if [ `uname` = "Darwin" ]; then
 file2=arch/Darwin_generic.mk
 sed -i '' 's/libtool/g++-mp-4.9/' $file2
 sed -i '' 's/:= cc/:= gcc-mp-4.9/' $file2
 sed -i '' 's/:= c++/:= g++-mp-4.9/' $file2
 sed -i '' 's/dlib_linkflags := -dynamic/dlib_linkflags := -dynamiclib/' $file2
fi


echo " * Configuring namdlite"    >> $DIR/$file
make config INSTALL=no    &> /dev/null
if (( $? )); then
    echo "  * Problem configuring NAMDLite with make config." >> $DIR/$file
    exit 1
fi


echo " * Making NAMDLite"    >> $DIR/$file
make > MakeReport.txt &> /dev/null
if (( $? )); then
 echo "  * Problem compiling NAMDLite."   >> $DIR/$file
 echo " "                                 >> $DIR/$file
 exit 1
else
 echo "Finished making NAMDLite."         >> $DIR/$file
 echo " "                                 >> $DIR/$file
 echo "Finished making NAMDLite."         >> $DIR/$upfile
 echo " "                                 >> $DIR/$upfile
 exit 0
fi
