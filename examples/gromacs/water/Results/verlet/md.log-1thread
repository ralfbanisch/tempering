GROMACS:    gmx mdrun, VERSION 5.0.2

GROMACS is written by:
Emile Apol         Rossen Apostolov   Herman J.C. Berendsen Par Bjelkmar       
Aldert van Buuren  Rudi van Drunen    Anton Feenstra     Sebastian Fritsch  
Gerrit Groenhof    Christoph Junghans Peter Kasson       Carsten Kutzner    
Per Larsson        Justin A. Lemkul   Magnus Lundborg    Pieter Meulenhoff  
Erik Marklund      Teemu Murtola      Szilard Pall       Sander Pronk       
Roland Schulz      Alexey Shvetsov    Michael Shirts     Alfons Sijbers     
Peter Tieleman     Christian Wennberg Maarten Wolf       
and the project leaders:
Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel

Copyright (c) 1991-2000, University of Groningen, The Netherlands.
Copyright (c) 2001-2014, The GROMACS development team at
Uppsala University, Stockholm University and
the Royal Institute of Technology, Sweden.
check out http://www.gromacs.org for more information.

GROMACS is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 2.1
of the License, or (at your option) any later version.

GROMACS:      gmx mdrun, VERSION 5.0.2
Library dir:  /usr/local/gromacs/share/gromacs/top
Command line:
  gmx mdrun -nt 1 -s mineMIST.tpr -mp water.top

Gromacs version:    VERSION 5.0.2
Precision:          single
Memory model:       64 bit
MPI library:        thread_mpi
OpenMP support:     disabled
GPU support:        disabled
invsqrt routine:    gmx_software_invsqrt(x)
SIMD instructions:  SSE2
FFT library:        fftw-3.3.3-sse2
RDTSCP usage:       enabled
C++11 compilation:  disabled
TNG support:        enabled
Tracing support:    disabled
Build OS/arch:      Linux 2.6.32-431.20.5.el6.x86_64 x86_64
Build CPU vendor:   AuthenticAMD
Build CPU brand:    AMD Opteron(tm) Processor 4162 EE
Build CPU family:   16   Model: 8   Stepping: 1
Build CPU features: apic clfsh cmov cx8 cx16 htt lahf_lm misalignsse mmx msr nonstop_tsc pdpe1gb popcnt pse rdtscp sse2 sse3 sse4a
C compiler:         /usr/bin/gcc GNU 4.4.7
C compiler flags:    -msse2   -D__MIST_WITH_GROMACS -Wno-maybe-uninitialized -Wextra -Wno-missing-field-initializers -Wno-sign-compare -Wpointer-arith -Wall -Wno-unused -Wunused-value -Wunused-parameter -Wno-unknown-pragmas  -O3 -DNDEBUG -fomit-frame-pointer -funroll-all-loops  -Wno-array-bounds 
C++ compiler:       /usr/bin/g++ GNU 4.4.7
C++ compiler flags:  -msse2   -D__MIST_WITH_GROMACS -Wextra -Wno-missing-field-initializers -Wpointer-arith -Wall -Wno-unused-function -Wno-unknown-pragmas  -O3 -DNDEBUG -fomit-frame-pointer -funroll-all-loops  -Wno-array-bounds 
Boost version:      1.55.0 (internal)



++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess and C. Kutzner and D. van der Spoel and E. Lindahl
GROMACS 4: Algorithms for highly efficient, load-balanced, and scalable
molecular simulation
J. Chem. Theory Comput. 4 (2008) pp. 435-447
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
D. van der Spoel, E. Lindahl, B. Hess, G. Groenhof, A. E. Mark and H. J. C.
Berendsen
GROMACS: Fast, Flexible and Free
J. Comp. Chem. 26 (2005) pp. 1701-1719
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
E. Lindahl and B. Hess and D. van der Spoel
GROMACS 3.0: A package for molecular simulation and trajectory analysis
J. Mol. Mod. 7 (2001) pp. 306-317
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
H. J. C. Berendsen, D. van der Spoel and R. van Drunen
GROMACS: A message-passing parallel molecular dynamics implementation
Comp. Phys. Comm. 91 (1995) pp. 43-56
-------- -------- --- Thank You --- -------- --------

Can not increase nstlist because an NVE ensemble is used
Input Parameters:
   integrator                     = mist
   tinit                          = 0
   dt                             = 0.001
   nsteps                         = 50
   init-step                      = 0
   simulation-part                = 1
   comm-mode                      = Linear
   nstcomm                        = 100
   bd-fric                        = 0
   emtol                          = 10
   emstep                         = 0.01
   niter                          = 20
   fcstep                         = 0
   nstcgsteep                     = 1000
   nbfgscorr                      = 10
   rtpi                           = 0.05
   nstxout                        = 10
   nstvout                        = 10
   nstfout                        = 0
   nstlog                         = 10
   nstcalcenergy                  = 0
   nstenergy                      = 10
   nstxout-compressed             = 0
   compressed-x-precision         = 1000
   cutoff-scheme                  = Verlet
   nstlist                        = 10
   ns-type                        = Simple
   pbc                            = xyz
   periodic-molecules             = FALSE
   verlet-buffer-tolerance        = 0.005
   rlist                          = 1.257
   rlistlong                      = 1.257
   nstcalclr                      = 10
   coulombtype                    = Cut-off
   coulomb-modifier               = Potential-shift
   rcoulomb-switch                = 0
   rcoulomb                       = 1.2
   epsilon-r                      = 1
   epsilon-rf                     = inf
   vdw-type                       = Cut-off
   vdw-modifier                   = Potential-shift
   rvdw-switch                    = 0
   rvdw                           = 1.2
   DispCorr                       = No
   table-extension                = 1
   fourierspacing                 = 0.12
   fourier-nx                     = 0
   fourier-ny                     = 0
   fourier-nz                     = 0
   pme-order                      = 4
   ewald-rtol                     = 1e-05
   ewald-rtol-lj                  = 0.001
   lj-pme-comb-rule               = Geometric
   ewald-geometry                 = 0
   epsilon-surface                = 0
   implicit-solvent               = No
   gb-algorithm                   = Still
   nstgbradii                     = 1
   rgbradii                       = 1
   gb-epsilon-solvent             = 80
   gb-saltconc                    = 0
   gb-obc-alpha                   = 1
   gb-obc-beta                    = 0.8
   gb-obc-gamma                   = 4.85
   gb-dielectric-offset           = 0.009
   sa-algorithm                   = Ace-approximation
   sa-surface-tension             = 2.05016
   tcoupl                         = No
   nsttcouple                     = -1
   nh-chain-length                = 0
   print-nose-hoover-chain-variables = FALSE
   pcoupl                         = No
   pcoupltype                     = Isotropic
   nstpcouple                     = -1
   tau-p                          = 1
   compressibility (3x3):
      compressibility[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      compressibility[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      compressibility[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   ref-p (3x3):
      ref-p[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      ref-p[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      ref-p[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   refcoord-scaling               = No
   posres-com (3):
      posres-com[0]= 0.00000e+00
      posres-com[1]= 0.00000e+00
      posres-com[2]= 0.00000e+00
   posres-comB (3):
      posres-comB[0]= 0.00000e+00
      posres-comB[1]= 0.00000e+00
      posres-comB[2]= 0.00000e+00
   QMMM                           = FALSE
   QMconstraints                  = 0
   QMMMscheme                     = 0
   MMChargeScaleFactor            = 1
qm-opts:
   ngQM                           = 0
   constraint-algorithm           = Lincs
   continuation                   = FALSE
   Shake-SOR                      = FALSE
   shake-tol                      = 0.0001
   lincs-order                    = 4
   lincs-iter                     = 1
   lincs-warnangle                = 30
   nwall                          = 0
   wall-type                      = 9-3
   wall-r-linpot                  = -1
   wall-atomtype[0]               = -1
   wall-atomtype[1]               = -1
   wall-density[0]                = 0
   wall-density[1]                = 0
   wall-ewald-zfac                = 3
   pull                           = no
   rotation                       = FALSE
   interactiveMD                  = FALSE
   disre                          = No
   disre-weighting                = Conservative
   disre-mixed                    = FALSE
   dr-fc                          = 1000
   dr-tau                         = 0
   nstdisreout                    = 100
   orire-fc                       = 0
   orire-tau                      = 0
   nstorireout                    = 100
   free-energy                    = no
   cos-acceleration               = 0
   deform (3x3):
      deform[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   simulated-tempering            = FALSE
   E-x:
      n = 0
   E-xt:
      n = 0
   E-y:
      n = 0
   E-yt:
      n = 0
   E-z:
      n = 0
   E-zt:
      n = 0
   swapcoords                     = no
   adress                         = FALSE
   userint1                       = 0
   userint2                       = 0
   userint3                       = 0
   userint4                       = 0
   userreal1                      = 0
   userreal2                      = 0
   userreal3                      = 0
   userreal4                      = 0
grpopts:
   nrdf:       36618
   ref-t:           0
   tau-t:           0
annealing:          No
annealing-npoints:           0
   acc:	           0           0           0
   nfreeze:           N           N           N
   energygrp-flags[  0]: 0
Using 1 MPI thread

Detecting CPU SIMD instructions.
Present hardware specification:
Vendor: AuthenticAMD
Brand:  AMD Opteron(tm) Processor 4162 EE
Family: 16  Model:  8  Stepping:  1
Features: apic clfsh cmov cx8 cx16 htt lahf_lm misalignsse mmx msr nonstop_tsc pdpe1gb popcnt pse rdtscp sse2 sse3 sse4a
SIMD instructions most likely to fit this hardware: SSE2
SIMD instructions selected at GROMACS compile time: SSE2

Cut-off's:   NS: 1.257   Coulomb: 1.2   LJ: 1.2
System total charge: 0.000

Using SSE2 4x4 non-bonded kernels

Using geometric Lennard-Jones combination rule

Potential shift: LJ r^-12: -1.122e-01 r^-6: -3.349e-01, Coulomb -8e-01
Removing pbc first time
Center of mass motion removal mode is Linear
We have the following groups for center of mass motion removal:
  0:  rest
There are: 12207 Atoms
Initial temperature: 299.983 K

           Step           Time         Lambda
              0        0.00000        0.00000

   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    3.12589e+04    8.94711e+03    9.68940e+04   -1.85162e+05   -4.80621e+04
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    5.87834e+04    1.07213e+04    3.86148e+02    2.51537e+03

           Step           Time         Lambda
             10        0.01000        0.00000

   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    1.49327e+04    1.61710e+04    4.63641e+04   -1.99712e+05   -1.22244e+05
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    1.42829e+05    2.05849e+04    9.38241e+02    3.43163e+04

           Step           Time         Lambda
             20        0.02000        0.00000

   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    8.05664e+03    1.81897e+04    4.34246e+04   -1.86910e+05   -1.17239e+05
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    1.45613e+05    2.83744e+04    9.56531e+02    6.51699e+04

           Step           Time         Lambda
             30        0.03000        0.00000

   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    1.11228e+04    1.33142e+04    4.49793e+04   -1.76979e+05   -1.07563e+05
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    1.35468e+05    2.79045e+04    8.89885e+02    4.33702e+04

           Step           Time         Lambda
             40        0.04000        0.00000

   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    2.12493e+04    3.85796e+04    4.51792e+04   -2.11343e+05   -1.06335e+05
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    1.29652e+05    2.33166e+04    8.51681e+02    2.61495e+03

           Step           Time         Lambda
             50        0.05000        0.00000



   Energies (kJ/mol)
           Bond          Angle        LJ (SR)   Coulomb (SR)      Potential
    3.13582e+04    6.28516e+03    4.53670e+04   -1.98828e+05   -1.15818e+05
    Kinetic En.   Total Energy    Temperature Pressure (bar)
    1.39269e+05    2.34512e+04    9.14856e+02   -1.84212e+04


	M E G A - F L O P S   A C C O U N T I N G

 NB=Group-cutoff nonbonded kernels    NxN=N-by-N cluster Verlet kernels
 RF=Reaction-Field  VdW=Van der Waals  QSTab=quadratic-spline table
 W3=SPC/TIP3p  W4=TIP4p (single or pairs)
 V&F=Potential and force  V=Potential only  F=Force only

 Computing:                               M-Number         M-Flops  % Flops
-----------------------------------------------------------------------------
 Pair Search distance check              37.381642         336.435     2.1
 NxN RF Elec. + LJ [F]                  188.330328        7156.552    44.9
 NxN RF Elec. + LJ [V&F]                 29.320088        1583.285     9.9
 NxN RF Electrostatics [F]              185.281272        5743.719    36.1
 NxN RF Electrostatics [V&F]             28.819192        1037.491     6.5
 Shift-X                                  0.085449           0.513     0.0
 Bonds                                    0.423176          24.967     0.2
 Angles                                   0.211588          35.547     0.2
 Virial                                   0.085764           1.544     0.0
 Stop-CM                                  0.024414           0.244     0.0
 Calc-Ekin                                0.146484           3.955     0.0
-----------------------------------------------------------------------------
 Total                                                   15924.252   100.0
-----------------------------------------------------------------------------


     R E A L   C Y C L E   A N D   T I M E   A C C O U N T I N G

On 1 MPI rank

